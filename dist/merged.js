var appgame = appgame || {};

appgame.Assets = (
	function(){

		var _instance = null;
		var _piso = null;  
		var _nave = null;
		var _assets = {} 
		var _loader = new THREE.JSONLoader();

		var _debug_ctype_text = true;

		function createTextCanvas(text, color, font, size) {
			size = size || 24;
			var canvas = document.createElement('canvas');
			var ctx = canvas.getContext('2d');
			var fontStr = (font || 'Arial') + ' ' + (size +'px');
			ctx.font = fontStr; 
			var w = ctx.measureText(text).width;
			var h = Math.ceil(size*1.25);
			canvas.width = w;
			canvas.height = h;
			ctx.font = fontStr;
			ctx.fillStyle = color || 'black';
			ctx.fillText(text, 0, size);
			return canvas;
		}
		function getCanvasTexture(canvas){
			var canvas_texture = new THREE.CanvasTexture(canvas);
			canvas_texture.playable = true;
			if(_debug_ctype_text){
				canvas_texture.ctype = "asset_canvas_texture";
			}
			return canvas_texture;
		}

		function getTextSprite(text,size){
			var texture_sprite =  new THREE.TextSprite({
			  	textSize: size, 
			  	redrawInterval: 250, 
			  	texture: {
			    	text: text,
			    	//fontFamily: 'Arial, Helvetica, sans-serif',
			    	fontFamily: 'Tahoma, Geneva, sans-serif',
			    	fontWeight: 'bold', 
			    	lineWidth: 0.1, 
			    	strokeStyle: '#666', 
			  	},
			  	material: {
			    	color: 0xdedede, 
			    	fog: true, 
			  	},
			});
			texture_sprite.playable = true;
			texture_sprite.visible = false;
			if(_debug_ctype_text){
				texture_sprite.ctype ="asset_texture_sprite";
			}
			return texture_sprite;
		}



		Assets = function(){

		};
		Assets.prototype = {
			constructor:Assets, 
			test:function(){
				console.log("hello from Assets object");
			},
			start:function(){  
				this.loadFile("piso","./modelos/piso.json",true);
				this.loadFile("nave","./modelos/nave.json");
				this.loadFile("bullet","./modelos/bullet.json");
			}, 
			showElements:function(){
				if(window.w_wgl && window.w_wgl.scene){
					for(var index in w_wgl.scene.children){
						var obj = w_wgl.scene.children[index];
						if(obj.playable === true){
							obj.visible = true;
						} 
					}
				}
			}, 
			hideElements:function(){
				if(window.w_wgl && window.w_wgl.scene){
					for(var index in w_wgl.scene.children){
						var obj = w_wgl.scene.children[index];
						if(obj.playable === true){
							obj.visible = false;
						} 
					}
				}
			},
			render:function(){ 
				if(!_piso && "piso" in _assets){
					_piso = _assets["piso"];
					_piso.rotateX(Math.PI/2);
					_piso.position.z = -0.1;
				}
				if(!_nave && "nave" in _assets){
					_nave = _assets["nave"];
					_nave.up.y=0; 
					_nave.up.z=1; 
				}
			},
			getNave:function(){
				console.log("retrieving nave");
				var _tmp = null;
				if(_nave){
					_tmp = _nave.clone();
					if(_debug_ctype_text){
						_tmp.ctype = "asset_nave";
						_tmp.playable = true;
					}
				}
				return _tmp;
			},
			get:function(_str){
				return _str in _assets ? _assets[_str] : null;
			},
			setLife:function(mesh){
				var canvas = document.createElement('canvas');
				var ctx = canvas.getContext('2d');
				canvas.width = canvas.height = 512;
				var texture = getCanvasTexture(canvas);
				texture.ctx = canvas.ctx = ctx;
				texture.canvas = canvas;

				/*
				var geometry = new THREE.PlaneGeometry( 1,1,2 );
				var material = new THREE.MeshBasicMaterial( {map:texture, side: THREE.FrontSide} );
				var plane = new THREE.Mesh( geometry, material );
				mesh.life_mesh = plane;
				mesh.life_mesh.texture = texture;
				*/

				var spriteMaterial = new THREE.SpriteMaterial( { map: texture, transparent:true } );

				mesh.life_mesh = new THREE.Sprite( spriteMaterial );
				mesh.life_mesh.texture = texture;
				mesh.life_mesh.ctype = "asset_sprite_life";
				mesh.life_mesh.playable = true;
				mesh.life_mesh.visible = false;
			},
			drawLife:function(mesh){ 
				if(typeof mesh.life =="undefined"){
					mesh.life = 100.0; 
				} 
				var lifeTexture = mesh.life_mesh.texture;
				var defFillStyle = lifeTexture.ctx.fillStyle;
				lifeTexture.ctx.fillStyle = "#FFFFFF";
				lifeTexture.ctx.fillRect(0,0,lifeTexture.canvas.width,60);
				lifeTexture.ctx.fillStyle = "#00FF00";
				var _w = lifeTexture.canvas.width*(mesh.life/100.0);
				lifeTexture.ctx.fillRect(0,0,_w,60); 
				lifeTexture.ctx.fillStyle = defFillStyle; 
				lifeTexture.needsUpdate = true;
				if(lifeTexture.version > 100000)lifeTexture.version = 1;
			},
			animate:function(){ 
			}, 
			loadFile:function(idx,obj,_append){
				_loader.load( 
					obj,  
					function ( geometry, materials ) { 
						var object;  
						/*for(var _index in materials){
							var _mat = materials[_index]; 
							_mat.side = THREE.FrontSide;
						} */
						/*
						for(var index in w_wgl.scene.children){
							var obj = w_wgl.scene.children[index];
							if(obj.playable && obj.visible){
								console.log(obj,obj.ctype,obj.type);
							} 
						}
						*/
						object = new THREE.Mesh( geometry, materials );  
						object.ctype = idx;
						if(idx!="piso"){
							object.playable = true;
						}
						if(_append){
							w_wgl.scene.add( object );  
						}
						_assets[idx] = object;
					},
					function(){

					},
					function(xhr){
						console.log(xhr);
					}
				);
			},
			getPlayerNameMesh:function(text){ 
			    /*
			    var texture = new THREE.Texture(createTextCanvas(text,'black','italic',20)); 
			    texture.needsUpdate = true;
			    var material = new THREE.MeshBasicMaterial({ map: texture,transparent:true});
			    var mesh = new THREE.Mesh( new THREE.PlaneGeometry(2,2,4,4), material );
			    mesh.rotateX(-2*Math.PI);
			    */
			    var mesh = getTextSprite(text,0.5);
			    mesh.text = text;
			    return mesh;
			},
		};

		return {
			instance:function(){
				if(!_instance){
					_instance = new Assets(); 
				}
				return _instance;
			}
		};
	}
)();

var appgame = appgame || {};

appgame.Enemies = (
	function(){

		var _instance = null;   
		var _enemies = [];
		var _assets = null;
		var _parent = null;
		var _bullets = [];
		Enemies = function(p){
			_parent = p;
			_assets = appgame.Assets.instance();
		};
		Enemies.prototype = {
			constructor:Enemies, 
			test:function(){
				console.log("hello from Enemies object");
			},
			start:function(){  
			}, 
			render:function(){ 
			},
			animate:function(){ 
			},
			parseBullets:function(import_bullets){
				var _bullet = _assets.get("bullet");
				if(!_bullet)return;
				var _idx,_idy;
				for(_idx=0;_idx<_bullets.length;_idx++){
					_bullets[_idx].visible = true;
				}
				if(import_bullets.length>_bullets.length){ 
					var _diff = import_bullets.length - _bullets.length;
					for(_idx=0;_idx<_diff;_idx++){
						var _b = _bullet.clone();
						_b.up.set(0,0,1);
						_bullets.push(_b);
						w_wgl.scene.add(_b);
					}
				}else if(import_bullets.length<_bullets.length){
					var _diff = _bullets.length - import_bullets.length;
					for(_idx=0;_idx<_diff;_idx++){
						_bullets[_idx].visible = false;
					}

				} 
				_idy=0;
				for(_idx=0;_idx<_bullets.length;_idx++){
					var _obj = _bullets[_idx];
					if(_obj.visible){
						var _ib = import_bullets[_idy];
						_obj.position.fromArray(_ib.position,0);
						if(_ib.lookAt){ 
							_obj.lookAt( 
								_ib.lookAt[0],
								_ib.lookAt[1],
								_ib.lookAt[2] 
							);
						}
						_idy++;
					}
				}
			},
			parseClients:function(_clients,_userid){ 
				var _import_bullets = [];
				var _client,_enemy;
				for(var _idx=0;_idx<_clients.length;_idx++){
					_client = _clients[_idx]; 
					if(_client.bullets){
						for(var _idy=0;_idy<_client.bullets.length;_idy++){
							_import_bullets.push(_client.bullets[_idy]); 
						}
					}
					if(_userid!=_client.userid){
						var _find = false;
						_enemy = null;
						for(var _idy=0;_idy<_enemies.length;_idy++){
							_enemy = _enemies[_idy];
							if(_enemy.userid == _client.userid){
								_find = true;
								break;
							}
						}
						if(!_find){ 
							_enemy = _assets.getNave();
							if(_enemy){ 
								_enemy.userid = _client.userid;
								_enemies.push(_enemy);
								_enemy.name_mesh = _assets.getPlayerNameMesh(_client.name);
								w_wgl.scene.add(_enemy.name_mesh);
								w_wgl.scene.add(_enemy);
							}
						}
						if(_enemy){ 
							if( _client.name!=_enemy.name_mesh.text && typeof _client.name =="string"){ 
								_enemy.name_mesh.text = _enemy.name_mesh.material.map.text = _client.name;
							}
							_enemy.position.fromArray(_client.position,0);
							_enemy.name_mesh.position.copy(_enemy.position); 	
							_enemy.lookAt(
									_client.lookAt[0],
									_client.lookAt[1],
									_client.lookAt[2]
								); 

							if(!_enemy.life_mesh){
								_assets.setLife(_enemy);
								if(_enemy.life_mesh){ 
									_enemy.life = _client.life;
									w_wgl.scene.add(_enemy.life_mesh); 
								}
							}
							if(_enemy.life_mesh && _assets){  
								_enemy.life = _client.life;
								_enemy.life_mesh.position.copy(_enemy.position);
								_assets.drawLife(_enemy);
								_enemy.life_mesh.visible = true;
							}
							if(_enemy.name_mesh){
								_enemy.name_mesh.visible = true;
							}
						}
					}else{
						_parent.setData(_client);
					}
				}
				this.parseBullets(_import_bullets);
				//delete
				var _ids_to_deleted = [];
				for(var _idy=0;_idy<_enemies.length;_idy++){
					_enemy = _enemies[_idy];
					_find = false;
					for(var _dx=0;_dx<_clients.length;_dx++){
						_client = _clients[_dx];
						if(_client.userid==_enemy.userid){
							_find = true;
							break;
						}
					}
					if(!_find){
						_ids_to_deleted.push(_idy);
					}
				} 
				for(var _idy=0;_idy<_ids_to_deleted.length;_idy++){  
					var _id = _ids_to_deleted[_idy];
					_enemy = _enemies[_id];
					if(_enemy.name_mesh){
						w_wgl.scene.remove(_enemy.name_mesh);
					}
					if(_enemy.life_mesh){
						w_wgl.scene.remove(_enemy.life_mesh);
					}
					w_wgl.scene.remove(_enemy);
					_enemies.splice(_id,1);
				} 
				delete _ids_to_deleted;
			}
		};

		return {
			instance:function(p){
				if(!_instance){
					_instance = new Enemies(p);
					return _instance;
				}
				return null;
			}
		};
	}
)();

var appgame = appgame || {};

appgame.FullScreen = (
	function(){

		var _instance = null; 

		var _btn_maximize_src = "./imgs/boton_maximize.png";
		var _btn_minimize_src = "./imgs/boton_minimize.png";

		var _btn_maximize = null;
		var _btn_minimize = null;  

		var _width = 90;
		var _height = 90;

		var _radius = 30;

		var _width2 = _radius*2;
		var _height2 = _radius*2;

		var _point = {x:10,y:10,offsetX:10,offsetY:10};

		var _fullscreen_btn_clicked = false;
		
		var _touch_identifier = null;

		FullScreen = function(){

		};
		FullScreen.prototype = {
			constructor:FullScreen, 
			test:function(){
				console.log("hello from FullScreen");
			},
			start:function(){
				this.cargarSprites();
				this.configurarPosicionSprites();
				this.setListener();
			},
			setListener:function(){
				var _this = this;
				if(w_ctouch){
					$(w_ctouch).click(
						function(_evt){
							_evt.preventDefault();
							_this.listen(_evt);
						}
					);
				}
				window.addEventListener(
					"fullscreenchange", 
					function(evt){ 
						_this.onChange(evt);
					}
				);
				window.addEventListener(
					"webkitfullscreenchange", 
					function(evt){ 
						_this.onChange(evt);
					}
				);
				window.addEventListener(
					"mozfullscreenchange", 
					function(evt){ 
						_this.onChange(evt);
					}
				);
				window.addEventListener(
					"fullscreenerror", 
					function(evt){ 
						_this.onError(evt);
					}
				);
				window.addEventListener(
					"webkitfullscreenerror", 
					function(evt){ 
						_this.onError(evt);
					}
				);
				window.addEventListener(
					"mozfullscreenerror", 
					function(evt){ 
						_this.onError(evt);
					}
				);
				
			},
			onChange:function(evt){
				//console.log("change",evt);
				w_ctouch._lib_fullscreen_clicked = false;
			},
			onError:function(evt){
				//console.log("error",evt);
				w_ctouch._lib_fullscreen_clicked = false;
			},
			listen:function(evt){ 
				var _v2a = new THREE.Vector2(_point.x+_width2/2,_point.y+_height2/2);
				var _v2b = new THREE.Vector2(evt.clientX,evt.clientY);
				if(_v2a.distanceTo(_v2b)<=_radius){ 
					w_ctouch._lib_fullscreen_clicked = true;
					this.toggleFullScreen();  
				}else{
					w_ctouch._lib_fullscreen_clicked = false;
				}
			},
			toggleFullScreen:function() {
			  	if (!this.isWindowFullScreen()) {
			  		document.documentElement.requestFullscreen = 
			  			document.documentElement.webkitRequestFullscreen || 
			  			document.documentElement.mozRequestFullscreen || 
			  			document.documentElement.requestFullscreen;
			      	document.documentElement.requestFullscreen();
			  	}else{
			  		document.exitFullscreen = 
			  			document.exitFullscreen || 
			  			document.mozExitFullscreen || 
			  			document.webkitExitFullscreen;
			    	if (document.exitFullscreen) {
			      		document.exitFullscreen(); 
			    	}
			  	}
			},
			render:function(){
				this.dibujarSprites();
			},
			isWindowFullScreen:function(){
				return w_width == screen.width && w_height == screen.height;
			},
			resize:function(){
				this.configurarPosicionSprites();
			},
			configurarPosicionSprites:function(){
				_point.x = w_width - _width2 - _point.offsetX;
				_point.y = _point.offsetY;
			},
			cargarSprites:function(){
				if(!w_ctouch_ctx)return; 
				_btn_maximize = new Image;
				_btn_maximize.onload=function(){
					_btn_maximize._loaded = true;
				};	
				_btn_maximize.src = _btn_maximize_src;
				_btn_minimize = new Image; 
				_btn_minimize.onload=function(){
					_btn_minimize._loaded = true;
				};	
				_btn_minimize.src = _btn_minimize_src; 
			},
			dibujarSprites:function(){ 
				if(this.isWindowFullScreen()){
					if(w_ctouch_ctx && _btn_minimize && _btn_minimize._loaded){  
						w_ctouch_ctx.beginPath();
						w_ctouch_ctx.drawImage(_btn_minimize,0,0,_width,_height,_point.x,_point.y,_width2,_height2);
						w_ctouch_ctx.closePath();
					} 
				}else{
					if(w_ctouch_ctx && _btn_maximize && _btn_maximize._loaded){  
						w_ctouch_ctx.beginPath();
						w_ctouch_ctx.drawImage(_btn_maximize,0,0,_width,_height,_point.x,_point.y,_width2,_height2);
						w_ctouch_ctx.closePath();
					} 	
				} 
			},
			touch:function(_what,_evt){
				if(_what=="s")this.touch_start(_evt);
				if(_what=="e")this.touch_end(_evt);
				if(_what=="c")this.touch_cancel(_evt);
				if(_what=="l")this.touch_leave(_evt);
				if(_what=="m")this.touch_move(_evt);
			},
			touch_point_collide:function(_touch){
				if(_touch.clientX && _touch.clientY){
					var _v2a = new THREE.Vector2(_point.x+_width2/2,_point.y+_height2/2);
					var _v2b = new THREE.Vector2(_touch.clientX,_touch.clientY);
					if(_v2a.distanceTo(_v2b)<=_radius){ 
						return true;
					}
				}
				return false;
			},
			touch_start:function(_evt){ 
				if(_touch_identifier===null){
					var _touches = _evt.changedTouches; 
					for(var _idx in _touches){
						var _touch = _touches[_idx];
						if(this.touch_point_collide(_touch)){
							_touch_play = true;
							_touch_identifier = _touch.identifier;
							break;
						}
					} 
				} 
			},
			touch_move:function(_evt){  
				if(_touch_identifier!==null){
					var _touches = _evt.changedTouches; 
					for(var _idx in _touches){
						var _touch = _touches[_idx];
						if(
							_touch_identifier==_touch.identifier 
						){
							if(this.touch_point_collide(_touch)){
								_touch_play = true; 
							}else{
								_touch_play = false;
							}
							break;
						} 
					} 
				} 
			},
			touch_end:function(_evt){  
				if(_touch_identifier!==null){
					var _touches = _evt.changedTouches; 
					for(var _idx in _touches){
						var _touch = _touches[_idx]; 
						if(_touch_identifier==_touch.identifier){
							_touch_play = false;
							_touch_identifier = null;
							break;
						}
					} 
				}  
			},
			touch_cancel:function(_evt){  
				this.touch_end(_evt);
			},
			touch_leave:function(_evt){  
				this.touch_end(_evt);
			}
		};

		return {
			instance:function(){
				if(!_instance){
					_instance = new FullScreen();
					return _instance;
				}
				return null;
			}
		};
	}
)();

var appgame = appgame || {};

appgame.LeaderBoard = (
	function(){

		var _instance = null; 
		var _assets = null;

		var _data = null;

		LeaderBoard = function(){ 
			_assets = appgame.Assets.instance();
		};
		LeaderBoard.prototype = {
			constructor:LeaderBoard, 
			test:function(){
				console.log("hello from LeaderBoard object");
			},
			start:function(){

			},
			setData:function(data){
				_data = data;
			},
			render:function(){
				if(!window.w_ctouch_ctx || !_data)return; 
				var _pos_x = 7*w_width/8;
				var _pos_y = 40; 
				if(_data.length>0){
					var _order = [];
					for(var idx in _data){
						_order.push([_data[idx].name,_data[idx].life]);
					}	
					_order.sort(function(a,b){
						return b[1] - a[1];
					});
					var _tmp_font = w_ctouch_ctx.font;
					var _max = 5;
					var _count = 1; 
					for(var idx in _order){
						if(_count>_max)break;  
						var _obj = _order[idx];
						w_ctouch_ctx.beginPath();
						w_ctouch_ctx.font="20px Georgia";
						var _txt = _count + ". "+_obj[0];
						w_ctouch_ctx.fillText(_txt,_pos_x,_pos_y);
						w_ctouch_ctx.closePath();
						_pos_y+=30;
						_count++;
					}
					w_ctouch_ctx.font=_tmp_font;
				} 
			}
		};


		return {
			instance:function(){
				if(!_instance){
					_instance = new LeaderBoard(); 
				}
				return _instance;
			}
		};
	}
)();
$(window).ready(
	function(evt){  
		setCanvasDim();
		$(window).resize(
			function(evt){
				setCanvasDim();
			}
		);
		(window.appMain = appgame.Main).start();
	}
); 
function setCanvasDim(){
	window.w_width = window.innerWidth;
	window.w_height = window.innerHeight;
	$("canvas").each(
		function(index,obj){
			var _this = $(obj);
			var _id = _this.attr("id");
			if(
				_id!="webgl" &&  
				_id!="canvas-assets" &&  
				_id!="canvas-touch" 
			){
				return;
			}
			_this.css("width",w_width+"px").css("height",w_height+"px");
			_this.attr("width",w_width).attr("height",w_height);
		}
	);
}

var appgame = appgame || {};
appgame.Main = (
	function(){

		var _instance = null;

		var _router = null;
		var _zona_a = null;
		var _ambientLight = new THREE.AmbientLight( 0xffffff); 
		var _directionalLight = new THREE.DirectionalLight( 0xaaaaaa, 0.1 ); 

		 

		var _cameraZPosInit = 10;
		var _cameraYPosInit = 0;
		var _cameraYPosInit_min = _cameraYPosInit;
		var _cameraXPosInit = 0;

		var _cameraAngle_def = 60;
		var _cameraAngle = _cameraAngle_def;

		var _fullScreen = null; 
		var _leaderBoard = null; 
		var _realTime = null; 
		var _assets = null; 
		var _enemies = null; 
		var _stats = null;

		_debug_touch = true;

		window._lib_game_finish = false;

		var _mesh = null;
		var _mesh_added = false;

		var _key_browser = "bkey";
		var _key_browser_time = "btime";
		var _interval_key_browser = null;

		var lang = {

			es:{
				title:"Multijugador",
				modal_title:"Multijugador",
				made_title:"Hecho en Perú",
				nickname:"Apodo",
				desc:"Este es un juego indie no oficial en modo desarrollo.",
				btn_play:"Jugar",
				tab_opened:"Mantiene una pestaña abierta",
			},
			en:{ 
				title:"Multiplayer Dev",
				modal_title:"Multiplayer Dev",
				made_title:"Made in Peru",
				nickname:"nickname",
				desc:"This is a unofficial indie game in dev mode.", 
				btn_play:"Play",
				tab_opened:"Keep an open tab",
			},
			br:{
				popup:{
					title:"Hello",
				},
			}, 
		};

		var _curr_language = lang["en"];

		Main = function(){ 

			window.w_wgl 		= document.getElementById("webgl");
 			window.w_cassets 	= document.getElementById("canvas-assets");
 			window.w_ctouch 	= document.getElementById("canvas-touch");
 			if(window.w_cassets && window.w_cassets.getContext)window.w_cassets_ctx = window.w_cassets.getContext("2d");
 			if(window.w_ctouch && window.w_ctouch.getContext)window.w_ctouch_ctx = window.w_ctouch.getContext("2d");

		};
		Main.prototype = {
			constructor:Main,
			start:function(){

				var _lang = navigator.language || navigator.userLanguage || "en-US";
				if(_lang){
					_lang = _lang.split("-");
					if(Array.isArray(_lang) && _lang.length==2){
						_lang = _lang[0];
						if(_lang=="es" || _lang=="en"/* || _lang=="br"*/){
							_curr_language = lang[_lang]; 
						}
					}
				}
				$(".modal_title").html(_curr_language.title);
				$(".modal_desc").html(_curr_language.desc);
				$("#made_title").attr("title",_curr_language.made_title);
				$("#nickname").attr("placeholder",_curr_language.nickname);
				$("#play").html(_curr_language.btn_play);
				$("#pp2>label").html(_curr_language.tab_opened);
				console.log("current language: "+_lang);

				if(!localStorage)return;
				var _it = localStorage.getItem(_key_browser);
				var _it_time = localStorage.getItem(_key_browser_time);
				var curr_time = (new Date()).getTime();
				if(!_it || (curr_time - _it_time)>60000){
					localStorage.setItem(_key_browser,Math.random());
					localStorage.setItem(_key_browser_time,(new Date()).getTime());
					_interval_key_browser = setInterval(function(){
						var _it = localStorage.getItem(_key_browser);
						if(!_it){
							localStorage.setItem(_key_browser,Math.random());
							localStorage.setItem(_key_browser_time,(new Date()).getTime());
						} 
					},500);
					$(window).on('beforeunload', function() {
						if(_interval_key_browser)clearInterval(_interval_key_browser);
						localStorage.removeItem(_key_browser);
						localStorage.removeItem(_key_browser_time);
					});
				}else{
					console.log("existe ota pestaña abierta.");
					$("#pp1").hide();
					$("#pp2").show();
					return;
				}


				window._lib_lvl = 1;
				var scene = new THREE.Scene();
				scene.background = new THREE.Color(0.5,0.5,0.9);
				var camera = new THREE.PerspectiveCamera( _cameraAngle, w_width / w_height, 0.1, 1000 );

				var renderer = new THREE.WebGLRenderer({canvas:w_wgl,antialias:true}); 
				renderer.setSize(w_width,w_height);
				renderer.setPixelRatio(window.devicePixelRatio);
    	
  				_directionalLight.position.set(0,0,10); 
  				_directionalLight.target = new THREE.Object3D();
  				_directionalLight.target.position.set(0,0,0);


				scene.add( _ambientLight );
				scene.add( _directionalLight ); 
				scene.add( _directionalLight.target ); 
				camera.position.set(_cameraXPosInit,_cameraYPosInit,_cameraZPosInit);
				camera.lookAt(new THREE.Vector3());

				w_wgl.scene = scene;
				w_wgl.camera = camera;
				w_wgl.renderer = renderer; 
				w_wgl.playing = false;
				w_wgl.rqsAnim = true;

				w_wgl._lib_currTime = Date.now();
				w_wgl._lib_lastTime = w_wgl._lib_currTime;

				w_wgl.popup_visible = true;
 

				var _this = this;
				$(window).resize(
					function(evt){
						_this.resize();
					}
				);
				$(window).focus(
					function(evt){
						_this.focus();
					}
				);
				$(window).blur(
					function(evt){ 
						_this.blur();
					}
				);

				w_ctouch.addEventListener("touchstart",_this.touchstart,false);
				w_ctouch.addEventListener("touchend",_this.touchend,false);
				w_ctouch.addEventListener("touchcancel",_this.touchcancel,false);
				w_ctouch.addEventListener("touchleave",_this.touchleave,false);
				w_ctouch.addEventListener("touchmove",_this.touchmove,false);

				_stats = new Stats();
				_stats.showPanel( 0 );
				document.body.appendChild( _stats.dom );

				_this.animate();
 
				(_fullScreen = appgame.FullScreen.instance()).start();  
				(_leaderBoard = appgame.LeaderBoard.instance()).start();  
				(_realTime = appgame.Realtime.instance()).start();  
				(_assets = appgame.Assets.instance()).start();  

				_this.showPresentation();
				_this.procesarNivelJuego();
 
				_this.parseVendor();
				$("#play").click(function(_evt){ 
					_instance.popupStatus("hide"); 
				}); 
				$("#settings").click(function(_evt){
					_instance.popupStatus("show");
				}); 
			  	$(window).keyup(function(_evt){
			  		//console.log("keyup",_evt.key);
			  		if(_evt.key=="Escape"){
			  			_instance.popupStatus("show");
			  		}
			  	});

			}, 
			popupStatus:function(st){
				if(st=="show"){
					$("#popup").show();
					w_wgl.popup_visible = true;
				}
				if(st=="hide"){
					$("#popup").hide();
					w_wgl.popup_visible = false;
				}
			},
			touchstart:function(_evt){  
			},
			touchend:function(_evt){  
			},
			touchcancel:function(_evt){  
			},
			touchleave:function(_evt){  
			},
			touchmove:function(_evt){  
			}, 
			animate:function(){
				if(w_wgl.rqsAnim){
					w_wgl.rqs_anim_id = requestAnimationFrame( _instance.animate );
				}  
				_stats.begin();
				if(!w_wgl._lib_currTime){
					w_wgl._lib_currTime = Date.now();
					w_wgl._lib_lastTime = w_wgl._lib_currTime; 
				}
				w_wgl._lib_lastTime = w_wgl._lib_currTime;
				w_wgl._lib_currTime = Date.now();

				if(w_ctouch_ctx)w_ctouch_ctx.clearRect(0,0,w_width,w_height);
				if(_fullScreen)_fullScreen.render(); 
				if(_leaderBoard)_leaderBoard.render(); 
				if(_assets)_assets.render();  
				if(_assets && !_mesh){
					_mesh = _assets.getNave();
					if(_mesh){
						_mesh.name_mesh = _assets.getPlayerNameMesh("unnamed"); 
					}
				}
				if(_mesh && !_mesh_added){
					_mesh_added = true;
					w_wgl.scene.add(_mesh);
					w_wgl.scene.add(_mesh.name_mesh);
				}
				if(_realTime)_realTime.render(_mesh); 
				w_wgl.renderer.render(w_wgl.scene, w_wgl.camera);
				_stats.end();
			},
			blur:function(){
				//this.stopAnimation();
			},
			focus:function(){
				this.playAnimation();
			}, 
			stopAnimation:function(){
				w_wgl.rqsAnim = false;
				if(w_wgl.rqs_anim_id){
					cancelAnimationFrame(w_wgl.rqs_anim_id);
				}
			},
			playAnimation:function(){
				if(w_wgl.rqs_anim_id)cancelAnimationFrame(w_wgl.rqs_anim_id);
				w_wgl.rqsAnim = true;
				this.animate();
			},
			resize:function(){
				w_wgl.camera.aspect = w_width / w_height;
    			w_wgl.camera.updateProjectionMatrix();
				w_wgl.renderer.setSize(w_width,w_height);
				if(_fullScreen)_fullScreen.resize();  
			},
			parseVendor:function(){
				if(!window._lib_vendor && w_wgl.renderer){
					var ctx = w_wgl.renderer.getContext();
					var info = w_wgl.renderer.extensions.get("WEBGL_debug_renderer_info");
					window._lib_vendor = {};
					window._lib_vendor.vendor 		= ctx.getParameter(ctx.VENDOR);
					window._lib_vendor.renderer 	= ctx.getParameter(ctx.RENDERER);
					window._lib_vendor.u_vendor 	= ctx.getParameter(info.UNMASKED_VENDOR_WEBGL);
					window._lib_vendor.u_renderer 	= ctx.getParameter(info.UNMASKED_RENDERER_WEBGL);

					$("#cpu_info1").html(_lib_vendor.vendor);
					$("#cpu_info2").html(_lib_vendor.renderer);
					$("#cpu_info3").html(_lib_vendor.u_vendor);
					$("#cpu_info4").html(_lib_vendor.u_renderer); 
					$("#cpu_info5").html("#Procesadores: "+navigator.hardwareConcurrency); 

				}
			},
			showPresentation:function(){  

				window._lib_game_paused = true; 
			},
			procesarNivelJuego:function(){ 
				this.cargarZonaA();
			},
			cancelarCargaActual:function(){
				this.eliminarObjetosDeEscena();
			},
			eliminarObjetosDeEscena:function(){
				if(w_wgl.scene){ 
				} 
			},
			cargarZonaA:function(){
				if(_zona_a)_zona_a.cargarElementos();
			}
		};

		return {
			start:function(){
				if(!_instance){
					_instance = new Main();
					_instance.start();
				}
			}
		};
	}
)();

var appgame = appgame || {};

appgame.Realtime = (
	function(){

		var _instance = null;  
		var _socket = null;
		var _data = null;
		var _mousePos = new THREE.Vector2();
		var _mousePosLast = new THREE.Vector2();
		var _mouse3DPos = new THREE.Vector3();
		var _enemies = null;
		var _userid = null;
		var _assets = null;
		var _leaderboard = null;

		var _moveFrontBack = 0;//1: front, -1 : back
		var _moveLeftRight = 0;//1: left, -1 : right

		var _keyW = false;
		var _keyS = false;
		var _keyA = false;
		var _keyD = false;
		var _keyV = false;
		var _keyR = false;

		var _shot = false;

		var _dataToSendServer = function(){
			if(!w_wgl.camera)return;
			_mouse3DPos.set(
			    (_mousePos.x / window.innerWidth) * 2 - 1,
			    - (_mousePos.y / window.innerHeight) * 2 + 1,
			    0.5
			);
			_mouse3DPos.unproject(w_wgl.camera); 
			_mouse3DPos.sub(w_wgl.camera.position); 
			_mouse3DPos.normalize(); 
			_moveFrontBack = 0;
			_moveLeftRight = 0;
			if(_keyW)_moveFrontBack+=1;
			if(_keyS)_moveFrontBack-=1;
			if(_keyA)_moveLeftRight+=1;
			if(_keyD)_moveLeftRight-=1;

			t = (-1)*w_wgl.camera.position.z/_mouse3DPos.z; 
			x = w_wgl.camera.position.x + t*_mouse3DPos.x;
			z = w_wgl.camera.position.z + t*_mouse3DPos.z;
			y = w_wgl.camera.position.y + t*_mouse3DPos.y; 
			_mouse3DPos.set(x,y,z); 
			if(_mousePosLast.x!=_mousePos.x && _mousePosLast.y!=_mousePos.y){ 
				var _msg = {
					onlookat:true,
					msg:[_mouse3DPos.x,_mouse3DPos.y,_mouse3DPos.z],
					mvFB:_moveFrontBack,
					mvLR:_moveLeftRight,
					v:_keyV,
					r:_keyR,
					shot:_shot,
				}; 
				socketSend(_msg,true);
				
			} 
		};
		function socketSend(data,_json){
			if(_socket && _socket.readyState == _socket.OPEN){
				_socket.send(_json ? JSON.stringify(data) : data);
			}
		}


		Realtime = function(){

		};
		Realtime.prototype = {
			constructor:Realtime, 
			test:function(){
				console.log("hello from Realtime object");
			},
			onConnected:function(data){ 
				_userid = data.userid; 
				_data = data;
				console.log(data);
		  		console.log( 'Connected successfully to the socket.io server. My server side ID is ' + data.userid );
			},
			start:function(onlySocket){ 
				if(!onlySocket){
					(_enemies = appgame.Enemies.instance(this)).start();  
					(_leaderboard = appgame.LeaderBoard.instance()).start();  
					_assets = appgame.Assets.instance(); 
				}
				if(window.location.protocol=="http:"){
					_socket = new WebSocket('ws://localhost:50001');
				}else{
					_socket = new WebSocket('wss://arquigames.pe:50001');
				} 
				_socket.onopen = function(_evt){ 
					console.log("open",_evt);
					_assets.showElements();
				};
				_socket.onerror = function(_evt){ 
					console.log("error",_evt);
					_assets.hideElements();
				};
				_socket.onmessage = function(_evt){ 
					try{
						var _res =JSON.parse(_evt.data);
						if(_res.onconnected){
							_instance.onConnected(_res.msg);
						}
						if(_res.onclients){
							_instance.parseClients(_res.msg);
						}
					}catch(exc){

					}
				};
				_socket.onclose = function(_evt){
					console.log("close",_evt);
					_assets.hideElements();
					setTimeout(function(){_instance.start(true)},5000);
				}; 
			  	if(!onlySocket){
				  	$(window).keypress(function(_evt){
				  		//console.log("keypress",_evt.key); 
				  		if(_evt.key=="w")_keyW = true;
				 		if(_evt.key=="s")_keyS = true;
				  		if(_evt.key=="a")_keyA = true;
				 		if(_evt.key=="d")_keyD = true;
				 		if(_evt.key=="v")_keyV = true;
				 		if(_evt.key=="r")_keyR = true;
				  	});
				  	$(window).keyup(function(_evt){
				  		//console.log("keyup",_evt.key);
				  		if(_evt.key=="w")_keyW = false;
				 		if(_evt.key=="s")_keyS = false;
				  		if(_evt.key=="a")_keyA = false;
				 		if(_evt.key=="d")_keyD = false;
				 		if(_evt.key=="v")_keyV = false;
				 		if(_evt.key=="r")_keyR = false;
				  	});
				  	$(window).mousemove(function(_evt){
				  		_mousePos.set(_evt.clientX,_evt.clientY);
				  	}); 
				  	$(window).mousedown(function(_evt){
				  		//console.log(_evt);
				  		if(_evt.which==1){ 
				  			if(!w_wgl.popup_visible){
				  				_shot = true;
				  				//console.log("emiting shoting");
				  			}
				  		}
				  	}); 
				  	$(window).mouseup(function(_evt){ 
				  		if(_evt.which==1){ 
				  			_shot = false;
				  		}
				  	}); 
					$("#nickname").keyup(function(evt){ 
						evt.preventDefault();
						evt.stopPropagation();
						var _obj = $(this);
						if(evt.which==13){
							$("#popup").hide();
							w_wgl.popup_visible = false;
							socketSend({name:_obj.val()},true);
						}
					}); 
					$("#play").click(function(_evt){  
						socketSend({name:$("#nickname").val()},true);
					});
				  	setInterval(_dataToSendServer,1000/60);
			  	}
			}, 
			parseClients:function(data){ 
				if(_leaderboard){ 
					_leaderboard.setData(data); 
				}
				if(_enemies){
					_enemies.parseClients(data,_userid);
				}
			},
			setData:function(data){
				_data = data;
			},
			render:function(_mesh){
				if(_data && _mesh){ 
					_mesh.userid = _userid; 
					_mesh.position.fromArray(_data.position);
					_mesh.visible = true;  
					w_wgl.camera.position.x =_mesh.position.x;
					w_wgl.camera.position.y =_mesh.position.y; 
					w_wgl.camera.lookAt(_mesh.position); 
					_mesh.lookAt(_mouse3DPos); 
					if(_mesh.name_mesh){
						if(_assets && _data.name!=_mesh.name_mesh.text && typeof _data.name =="string"){
					 		_mesh.name_mesh.text = _mesh.name_mesh.material.map.text = _data.name; 
						}
						_mesh.name_mesh.position.copy(_mesh.position);
						_mesh.name_mesh.visible = true;
					}
					if(!_mesh.life_mesh){
						_assets.setLife(_mesh);
						if(_mesh.life_mesh){ 
							_mesh.life = _data.life;
							w_wgl.scene.add(_mesh.life_mesh);  
						}
					}else{
						_mesh.life_mesh.visible = true;
					}
					if(_mesh.life_mesh && _assets){  
						_mesh.life = _data.life;
						_mesh.life_mesh.position.copy(_mesh.position);
						_assets.drawLife(_mesh);
					}
				}
				if(_enemies)_enemies.render();  
			},
			animate:function(){ 
			}  
		};

		return {
			instance:function(){
				if(!_instance){
					_instance = new Realtime();
					return _instance;
				}
				return null;
			}
		};
	}
)();
