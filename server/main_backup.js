  
    var cluster = require('cluster');
    var numCPUs = require('os').cpus().length;
    console.log("#cpus: "+numCPUs);
    var redis = require("redis"),
    redisClient = redis.createClient({host:"172.17.0.2",port:"6379"});
    redisClient.on("error", function (err) {
        console.log("Error redis: " + err);
    });     



    if(cluster.isMaster){
        console.log("cluster is master");
        for(var _idx=0;_idx<numCPUs;_idx++){
            cluster.fork();
        }
        cluster.on('exit', (worker, code, signal) => {
            console.log("worker:",worker);
            console.log("code:",code);
            console.log("signal:",signal);
            cluster.fork();
        });
    }else{
        var gameport = 50001;  
        var UUID    = require('node-uuid');
        var THREE = require("three-js")(); 

        var ws = require("nodejs-websocket");

        var _max_catch_exception = 50;
        var _count_catch_exception = 0;
        var _sockets = [];  
      
        var server = ws.createServer(function (conn) {
            _onConnection(conn);
            console.log("#sockets : "+_sockets.length);
            conn.on("text", function (str) { 
                try{
                    var _json = JSON.parse(str); 
                    conn.props.shot = _json.shot || false;
                    if(_json.name)conn.props.player_name = _json.name; 
                    if(_json.onlookat){
                        conn.props.lookAt.x = _json.msg[0];
                        conn.props.lookAt.y = _json.msg[1];
                        conn.props.lookAt.z = 0;  
                    }
                    conn.props.mvFB = _json.mvFB || 0;
                    conn.props.mvRL = _json.mvRL || 0;
                    conn.props.flagVelocity = _json.v || false;
                }catch(e){
                    if(_count_catch_exception<_max_catch_exception){
                        console.log(e);
                        _count_catch_exception++;
                    } 
                }
            })
            conn.on("binary", function (inStream) { 
                var data = new Buffer(0); 
                inStream.on("readable", function () {
                    var newData = inStream.read();
                    if (newData){
                        data = Buffer.concat([data, newData], data.length+newData.length);
                    }
                });
                inStream.on("end", function () {
                    console.log("Received " + data.length + " bytes of binary data"); 
                })
            });
            conn.on("close", function (code, reason) {
                console.log("Connection closed",code,reason);
                console.log('\t socket disconnected ' + conn.userid ); 
                for(var _idx=0;_idx<_sockets.length;_idx++){
                    var _socket = _sockets[_idx];
                    if(_socket.userid == conn.userid){
                        _sockets.splice(_idx,1);
                    }
                }
                console.log("#sockets after any closed: "+_sockets.length);
            });
            conn.on("error", function (err) {
                //console.log(err); 
            });
        }).listen(gameport); 
        function _onConnection(socket){
            _sockets.push(socket);
            console.log("#sockets : "+_sockets.length);
            setProperties(socket);
            var _msg = { 
                    onconnected:true,
                    msg:{
                        userid: socket.props.userid,
                        position:[
                            socket.props.position.x,
                            socket.props.position.y,
                            0
                        ] ,
                        lookAt:[
                            socket.props.lookAt.x,
                            socket.props.lookAt.y,
                            0
                        ],
                        bullets:[] 
                    }
                };
            var _json = JSON.stringify(_msg); 
            socket.send(
                _json
            ); 
            console.log('\t socket.io:: player ' + socket.props.userid + ' connected, at pid:'+process.pid);  
        }
        function createNewBullet(socket){
            var _bullet = null;
            var _find = false;
            for(var _idx= 0; _idx<socket.props.bullets.length;_idx++){
                _bullet = socket.props.bullets[_idx];
                if(!_bullet.visible){
                    _find = true;
                    break;
                }
            }
            if(!_find){
                _bullet = {};
                socket.props.bullets.push(_bullet);
            }
            _bullet.visible = true;
            _bullet.position = socket.props.position.clone();
            _bullet.direction = socket.props.vUnit.clone();
            _bullet.velocity = 0.35;
            _bullet.lookAt = new THREE.Vector3();
            console.log("bullet created, #total : "+socket.props.bullets.length);
        } 
        function setProperties(socket){
            var _posX =THREE.Math.randFloat(-10,10);
            var _posY =THREE.Math.randFloat(-10,10);
            socket.props = {
                userid:UUID(),
                bullets:[],
                interval_newBullet_def:10,
                interval_newBullet:10,
                shot:false,
                player_name:'',
                flagVelocity:false,
                mvFB:0,
                mvRL:0,
            }; 
            socket.props.player_name = "Sin nombre";
            socket.props.position = new THREE.Vector3(_posX,_posY,0);
            socket.props.lookAt = new THREE.Vector3(_posX,_posY-1,0);
            socket.props.vUnit = new THREE.Vector3(0,-1,0);
            socket.props.velocity = 0;
            socket.props.points = {
                front:socket.props.position.clone().addScaledVector(socket.props.vUnit,1),
                back:socket.props.position.clone().addScaledVector(socket.props.vUnit,-1), 
            };
            redisClient.set("player:"+socket.props.userid,JSON.stringify(socket.props));
            redisClient.lpush("players",socket.props.userid);
        };
        function checkBulletOutside(bullet){
            if(bullet.position.x>100 || bullet.position.x<-100)bullet.visible = false;
            if(bullet.position.y>100 || bullet.position.y<-100)bullet.visible = false;
        };
        function checkSocketOutside(socket){
            if(socket.props.position.x>70)socket.props.position.x=70;
            if(socket.props.position.x<-70)socket.props.position.x=-70;
            if(socket.props.position.y>70)socket.props.position.y=70;
            if(socket.props.position.y<-70)socket.props.position.y=-70;
        };
        function setNewPosition(_socket,_pack){
            _pack.lookAt = _socket.props.lookAt.toArray(); 
            _socket.props.vUnit = _socket.props.lookAt.clone().sub(_socket.props.position).normalize();
            var _distance = _socket.props.lookAt.distanceTo(_socket.props.position);
            _distance = THREE.Math.clamp(_distance,0,10);
            if(_distance>0.1){
                _socket.props.velocity = 0.1*(_distance/10);
            }else{
                _socket.props.velocity = 0;
            }
            _socket.props.position.addScaledVector(_socket.props.vUnit,_socket.props.velocity);
            checkSocketOutside(_socket);

            _pack.position = _socket.props.position.toArray();
        };
        function setNewPosition2(_socket,_pack){
            _pack.lookAt = _socket.props.lookAt.toArray(); 
            _socket.props.vUnit = _socket.props.lookAt.clone().sub(_socket.props.position).normalize();
            var _distance = _socket.props.lookAt.distanceTo(_socket.props.position);
            _distance = THREE.Math.clamp(_distance,0,10);
            if(_distance>0.1){
                _socket.props.velocity = _socket.props.flagVelocity ? 0.1:0.05;
            }else{
                _socket.props.velocity = 0;
            }
            _socket.props.position.addScaledVector(_socket.props.vUnit,_socket.props.velocity);
            checkSocketOutside(_socket);

            _pack.position = _socket.props.position.toArray();
        };
        function setBulletStatus(_socket){
            if(_socket.props.interval_newBullet>0){
                _socket.props.interval_newBullet--;
            }else{
                if(_socket.props.shot){
                    createNewBullet(_socket); 
                }
                _socket.props.interval_newBullet = _socket.props.interval_newBullet_def;
            }
        };
        function getSocketBullets(_socket){
            var _arr_bullets = [];

            for(var _idy=0;_idy<_socket.props.bullets.length;_idy++){
                var _bullet = _socket.props.bullets[_idy];
                if(_bullet.visible){
                    _bullet.lookAt.copy(_bullet.position).addScaledVector(_bullet.direction,1); 
                    _bullet.position.addScaledVector(_bullet.direction,_bullet.velocity);
                    checkBulletOutside(_bullet);
                }
                if(_bullet.visible){ 
                    _arr_bullets.push({
                        position:_bullet.position.toArray(),
                        direction:_bullet.direction.toArray(),
                        lookAt:_bullet.lookAt.toArray() 
                    });
                }
            }
            return _arr_bullets;
        };

        var _fps = function(){
            var _packs = [];
            for(var _idx=0;_idx<_sockets.length;_idx++){
                var _socket = _sockets[_idx];
                if(_socket.readyState == _socket.OPEN){
                    setBulletStatus(_socket);
                    var _pack = {};
                    _pack.name = _socket.props.player_name;
                    _pack.userid = _socket.props.userid;
                    setNewPosition2(_socket,_pack);  
                    _pack.bullets = getSocketBullets(_socket); 
                    _packs.push(_pack);
                }else{
                    _sockets.splice(_idx,1);
                }
            }
            if(_packs.length>0){
                _packs = {onclients:true,msg:_packs};
                for(var _idx=0;_idx<_sockets.length;_idx++){
                    _sockets[_idx].sendText(JSON.stringify(_packs));
                } 
                delete _packs;
            } 
        };
        var _interval = setInterval(_fps,1000/60); 
        console.log("Worker "+process.pid+" started");
    } 
    