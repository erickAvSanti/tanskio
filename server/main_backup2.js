  
    var cluster = require('cluster');
    var numCPUs = require('os').cpus().length;
    console.log("#cpus: "+numCPUs);
    var redis = require("redis"),
    redisClient = redis.createClient({host:"172.17.0.2",port:"6379"});
    redisClient.on("error", function (err) {
        console.log("Error redis: " + err);
    });     



    if(cluster.isMaster){
        console.log("cluster is master");
        for(var _idx=0;_idx<numCPUs;_idx++){
            cluster.fork();
        }
        cluster.on('exit', (worker, code, signal) => {
            console.log("worker:",worker);
            console.log("code:",code);
            console.log("signal:",signal);
            cluster.fork();
        });
    }else{
        var gameport = 50001;  
        var UUID    = require('node-uuid');
        var THREE = require("three-js")(); 

        var ws = require("nodejs-websocket");

        var _max_catch_exception = 50;
        var _count_catch_exception = 0;
        var _sockets = [];  
        function parseReply(conn,reply){
            try{
                var reply = JSON.parse(reply); 
                for(var idx in conn.props){
                    if(typeof conn.props[idx]=="number" && idx in reply){
                        conn.props[idx] = reply[idx];
                    }
                    if(typeof conn.props[idx]=="string" && idx in reply){
                        conn.props[idx] = reply[idx];
                    }
                    if(typeof conn.props[idx]=="boolean" && idx in reply){
                        conn.props[idx] = reply[idx];
                    }
                    if(typeof conn.props[idx]=="object" && conn.props[idx] instanceof THREE.Vector3 && idx in reply){
                        conn.props[idx].x = reply[idx].x;
                        conn.props[idx].y = reply[idx].y;
                        conn.props[idx].z = reply[idx].z;
                    }
                }
            }catch(e){}
        }
        function callback_hmset(err,res){
            if(err)console.log("ERROR: "+err);
        }
      
        var server = ws.createServer(function (conn) {
            _onConnection(conn); 
            conn.on("text", function (str) { 
                redisClient.hgetall("p:"+conn.props.userid,function(err,reply){//reply is json
                    if(err){
                        console.log(err);
                        return;
                    }
                    if(!reply)return;
                    //parseReply(conn,reply);
                    try{ 
                        var _json = JSON.parse(str); 
                        conn.props.shot = _json.shot || false;
                        if(_json.name)conn.props.player_name = _json.name; 
                        if(_json.onlookat){ 
                            conn.props.lookAt.x = _json.msg[0];
                            conn.props.lookAt.y = _json.msg[1];
                            conn.props.lookAt.z = 0;  
                            redisClient.hmset("p_lookAt:"+conn.props.userid,[
                                    "x",conn.props.lookAt.x,
                                    "y",conn.props.lookAt.y,
                                    "z",conn.props.lookAt.z,
                                ],callback_hmset);
                        }
                        conn.props.mvFB = _json.mvFB || 0;
                        conn.props.mvRL = _json.mvRL || 0;
                        conn.props.flagVelocity = _json.v || false;
                        redisClient.hmset("p:"+conn.props.userid,[
                                "mvFB",conn.props.mvFB,
                                "mvRL",conn.props.mvRL,
                                "flagVelocity",conn.props.flagVelocity,
                                "updated_at",(new Date()).getTime(),
                            ],callback_hmset);
                    }catch(e){
                        if(_count_catch_exception<_max_catch_exception){
                            console.log(e);
                            _count_catch_exception++;
                        } 
                    }
                });
            }) 
            conn.on("close", function (code, reason) {
                console.log("Connection closed",code,reason);
                console.log('\t socket disconnected ' + conn.props.userid ); 
                for(var _idx=0;_idx<_sockets.length;_idx++){
                    var _socket = _sockets[_idx];
                    if(_socket.userid == conn.props.userid){
                        _sockets.splice(_idx,1);
                        redisClient.srem("players",conn.props.userid);
                        redisClient.del("p:"+conn.props.userid);
                        redisClient.del("p_position:"+conn.props.userid);
                        redisClient.del("p_lookAt:"+conn.props.userid);
                        redisClient.del("p_vUnit:"+conn.props.userid);
                        redisClient.del("p_bullets:"+conn.props.userid);
                        break;
                    }
                } 
            });
            conn.on("error", function (err) {
                console.log("error: "+conn.props.userid,err); 
            });
        }).listen(gameport); 
        function _onConnection(socket){
            _sockets.push(socket); 
            setProperties(socket);
            var _msg = { 
                    onconnected:true,
                    msg:{
                        userid: socket.props.userid,
                        position:[
                            socket.props.position.x,
                            socket.props.position.y,
                            0
                        ] ,
                        lookAt:[
                            socket.props.lookAt.x,
                            socket.props.lookAt.y,
                            0
                        ],
                        bullets:[] 
                    }
                };
            var _json = JSON.stringify(_msg); 
            socket.send(
                _json
            ); 
            console.log('\t socket.io:: player ' + socket.props.userid + ' connected, at pid:'+process.pid);  
        } 
        function setProperties(socket){
            var _posX =THREE.Math.randFloat(-10,10);
            var _posY =THREE.Math.randFloat(-10,10);
            socket.props = {
                userid:UUID(), 
                interval_newBullet_def:10,
                interval_newBullet:10,
                shot:false,
                player_name:'',
                flagVelocity:false,
                mvFB:0,
                mvRL:0,
                bullets:[],
                created_at:(new Date()).getTime(),
                updated_at:(new Date()).getTime(),
            }; 
            socket.props.player_name = "Sin nombre";
            socket.props.position = new THREE.Vector3(_posX,_posY,0);
            socket.props.lookAt = new THREE.Vector3(_posX,_posY-1,0);
            socket.props.vUnit = new THREE.Vector3(0,-1,0);
            socket.props.velocity = 0; 
            console.log("setting properties for: "+socket.props.userid);
            var _arr = [];
            for(idx in socket.props){
                var _val = socket.props[idx];
                if(typeof _val !=="object"){
                    _arr.push(idx,_val);
                }else{
                    if(_val instanceof THREE.Vector3){
                        redisClient.hmset("p_"+idx+":"+socket.props.userid,[
                                "x",_val.x,
                                "y",_val.y,
                                "z",_val.z
                            ],callback_hmset);
                    }
                }
            } 

            redisClient.hmset(
                "p:"+socket.props.userid,
                _arr,
                callback_hmset
                );
            redisClient.sadd(
                "players",
                socket.props.userid,
                function(err,res){
                    console.log(err,res);
                });
        }; 
        function getBulletsPrimitiveData(_bullet_id,_socket_id,_set_packs){
            redisClient.hgetall("bullet:"+_bullet_id,function(err,res){
                if(err)throw err;
                if(res){ 
                    //_set_packs[_socket_id].bullets[_bullet_id].position = '';
                    //_set_packs[_socket_id].bullets[_bullet_id].direcion = '';
                    _set_packs[_socket_id].bullets[_bullet_id].visible = typeof res.visible =="boolean" ? res.visible : (res.visible==1 ? true:false);
                    _set_packs[_socket_id].bullets[_bullet_id].velocity = res.velocity;
                    //_set_packs[_socket_id].bullets[_bullet_id].lookAt = '';
                }
            });
        };
        function getForVector3Bullet(key,_bullet_id,_socket_id,_set_packs){ 
            redisClient.hgetall(`b_${key}:`+_bullet_id,function(err,res){
                if(err)throw err;
                if(res){
                    _set_packs[_socket_id].bullets[key] = [
                        res.x,
                        res.y,
                        res.z,
                    ];
                }
            });
        };
        function getBulletsObjectData(_bullet_id,_socket_id,_set_packs){
            var _arr =[
                "position",
                "direction",
                "lookAt",
            ]; 
            for(var idx in _arr)getForVector3Bullet(_arr[idx],_bullet_id,_socket_id,_set_packs);
        };
        function getSocketBulletsID(_socket_id,_set_packs){
            redisClient.smembers("p_bullets:"+_socket_id,function(err,res){
                if(err)throw err;
                if(res && res.length>0){
                    for(var idx in res){
                        var _bullet_id = res[idx]; 
                        _set_packs[_socket_id].bullets[_bullet_id] = {};
                        getBulletsPrimitiveData(_bullet_id,_socket_id,_set_packs);
                        getBulletsObjectData(_bullet_id,_socket_id,_set_packs);
                    }
                }             
            });
        };
        function getSocketPrimitiveData(_socket_id,_set_packs){
            redisClient.hgetall("p:"+_socket_id,function(err,res){
                if(err)throw err;
                if(res){
                    _set_packs[_socket_id].player_name = "player_name" in res ? res.player_name : "Sin nombre";
                    _set_packs[_socket_id].userid = res.userid;
                }
            });
        };
        function getForVector3Player(key,_socket_id,_set_packs){ 
            redisClient.hgetall(`p_${key}:`+_socket_id,function(err,res){
                if(err)throw err;
                if(res){
                    _set_packs[_socket_id][key] = [
                        res.x,
                        res.y,
                        res.z,
                    ];
                }
            });
        };
        function getSocketObjectData(_socket_id,_set_packs){
            var _arr =[
                "position",
                "lookAt",
            ]; 
            for(var idx in _arr)getForVector3Player(_arr[idx],_socket_id,_set_packs);
        };
        /*var _fps = function(){  
 
            var _packs = [];
            var _set_packs = {};

            redisClient.smembers("players",function(err,res){
                if(err)throw err;
                if(res && res.length>0){
                    for(var idx in res){
                        var _socket_id = res[idx];
                        _set_packs[_socket_id] = {
                            bullets:{}
                        }; 
                        getSocketBulletsID(_socket_id,_set_packs);
                        getSocketPrimitiveData(_socket_id,_set_packs);
                        getSocketObjectData(_socket_id,_set_packs);
                    }
                }
            });



            for(var _idx=0;_idx<_sockets.length;_idx++){
                var _socket = _sockets[_idx];
                if(_socket.readyState == _socket.OPEN){
                    //setBulletStatus(_socket);
                    var _pack = {};
                    _pack.name = _socket.props.player_name;
                    _pack.userid = _socket.props.userid;
                    setNewPosition2(_socket,_pack);  
                    _pack.bullets = getSocketBullets(_socket); 
                    _packs.push(_pack);
                }else{
                    _sockets.splice(_idx,1);
                }
            }
            if(_packs.length>0){
                _packs = {onclients:true,msg:_packs};
                for(var _idx=0;_idx<_sockets.length;_idx++){
                    _sockets[_idx].sendText(JSON.stringify(_packs));
                } 
                delete _packs;
            }  
        };
        var _interval = setInterval(_fps,1000/60); */
        console.log("Worker "+process.pid+" started");
    } 
    