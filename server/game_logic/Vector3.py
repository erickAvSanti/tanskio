import math

class Vector3:
  def __init__(self, x=0.0, y=0.0, z=0.0):
    self.x = x
    self.y = y
    self.z = z
    
  def copy(self,v):
    self.x = v.x
    self.y = v.y
    self.z = v.z
    return self 
  def clone(self):
    v = Vector3();
    v.copy(self);
    return v
  def __repr__(self):
    return "(" + self.toString() + ")"
  def __str__(self):
    return "(" + self.toString() + ")"
  
  def toString(self):
    return str(self.x) + "," + str(self.y) + "," + str(self.z)
    
  def length(self):
    return math.sqrt(self.x ** 2 + self.y ** 2 + self.z ** 2)
    
  def add(self, v):
    self.x += v.x
    self.y += v.y
    self.z += v.z
    return self
    
  def sub(self, v):
    self.x -= v.x
    self.y -= v.y
    self.z -= v.z
    return self

  def multiplyScalar(self, n):
    self.x *= n
    self.y *= n
    self.z *= n
    return self


  def divideScalar(self, n):
    return self.multiplyScalar(1.0/n)

  def fromArray(self,arr):
    self.x = float(arr[0])
    self.y = float(arr[1])
    self.z = float(arr[2])

  def normalize(self):
    _len = self.length()
    return self.divideScalar(_len if _len>0 else 1)
    
  def dot(self,v):
    return (v.x * self.x + v.y * self.y + v.z * self.z)

  def distanceToSquared(self,v):
    dx = self.x - v.x
    dy = self.y - v.y
    dz = self.z - v.z 
    return dx * dx + dy * dy + dz * dz

  def distanceTo(self,v):
    return math.sqrt( self.distanceToSquared( v ) );
    
  # def __div__(self, n):
  #   n /= -1
  #   return self * n

  def addScaledVector(self, v, s ):

    self.x += v.x * s;
    self.y += v.y * s;
    self.z += v.z * s;

    return self

  def toArray(self):
    return [self.x,self.y,self.z]
    
  @staticmethod
  def dot_product(v1, v2):
    return (v1.x * v2.x + v1.y * v2.y + v1.z * v2.z) 