import redis
import sys
import threading
import uuid
import json
import types
import datetime
import calendar
import time
import os.path
import re

from Vector3 import Vector3
from Bullet import Bullet
from Player import Player
from math import *

def isFloat(val): 
	cond = False
	if type(val) is str and re.match(r"^\-?(\d+\.\d+|\d+\.|\.\d+|\d+)$",val) is not None: 
		cond = True
	elif type(val) is int:
		cond =True
	elif type(val) is float:
		cond =True
	else:
		pass 
	return cond

def getTime():
	return int(time.time() * 1000)





_key_file = "../../../../../../../etc/ssl/ssl.key/arquigamespe.key";
_crt_file = "../../../../../../../etc/ssl/ssl.crt/arquigames_pe.crt";

if os.path.exists(_key_file) and os.path.exists(_crt_file):
	redisClient_0 = redis.StrictRedis(host='127.0.0.1', port=6379, db=0)
	redisClient_1 = redis.StrictRedis(host='127.0.0.1', port=6379, db=1) 
else:
	redisClient_0 = redis.StrictRedis(host='172.17.0.2', port=6379, db=0)
	redisClient_1 = redis.StrictRedis(host='172.17.0.2', port=6379, db=1) 




wait = False 
players = {}
bullets = {}
fps = 1/60.0
milli_sec = 0
#debug = True
debug = False

def restore():
	global players
	global bullets
	global redisClient_1
	global debug
	gameValues = redisClient_1.get("gameValues") 
	if gameValues is None:
		return
	gameValues = json.loads(gameValues,encoding="utf-8")
	if debug:
		print(gameValues)
		return
	count = 0
	for data in gameValues:
		player = Player()
		player.player_name 	= data['name']
		player.userid 		= data['userid']
		player.life 		= data['life']
		player.position.fromArray(data['position'])
		player.lookAt.fromArray(data['lookAt']) 
		player.created_at 	= int(data['created_at']) if 'created_at' in data else 0
		player.updated_at 	= int(data['updated_at']) if 'updated_at' in data else 0 
		bullets_info 		= data['bullets']
		player.resurrect 	= True  
		player.flagStarted 	= True if 'flagStarted' in data and (data['flagStarted']=='1' or data['flagStarted']==1) else False
		player.flagKill 	= True if 'flagKill' in data and (data['flagKill']=='1' or data['flagKill']==1) else False
		players[player.userid] = player
		for bullet_info in bullets_info:
			bullet = Bullet()
			bullet.id = bullet_info['id']
			bullet.position.fromArray(bullet_info['position'])
			bullet.direction.fromArray(bullet_info['direction'])
			bullet.lookAt.fromArray(bullet_info['lookAt'])

			player.bullets[bullet.id] = bullet
			bullets[bullet.id] = bullet
			bullet.player_id 	= player.userid

		count+=1

	for key,val in players.items():
		print(val.toString())
		print()

def render():
	global milli_sec
	global redisClient_0
	global players
	global wait
	global debug
	wait = True
	try:
		players_ids = redisClient_0.smembers("players") 
	except redis.exceptions.ConnectionError as ce:
		print("Error de conexion a redis",ce)
		sys.exit()

	object_keys = ['position','lookAt']

	for player_id in players_ids:
		player_info = redisClient_0.hgetall("p:"+player_id)  
		if player_info is None:
			continue
		if debug:
			for p_key,p_val in player_info.items():
				print(p_key,p_val,type(p_val))
			continue
		if player_id in players:
			player = players[player_id]
		else:
			player = Player()
			players[player_id] = player
		for p_key,p_val in player_info.items():
			
			if(p_key in object_keys):
				arr_values = p_val.split(",") 
				if len(arr_values)==3 and isFloat(arr_values[0]) and isFloat(arr_values[1]) and isFloat(arr_values[2]):
					if(p_key=="position"):
						if player.position_once and player.position_once_flag==0:
							player.position.fromArray(arr_values)  
							player.position_once_flag = 1
					if(p_key=="lookAt"):
						player.lookAt.fromArray(arr_values)  
			else: 
				if p_key.startswith('flag'):
					p_val = True if (p_val=='1' or p_val==1) else False
				else:
					if type(p_val) is unicode:
						p_val = p_val.encode('utf-8')
					else:
						if type(p_val) is int:
							p_val = int(p_val)
						else:
							if type(p_val) is float:
								p_val = float(p_val) 
				
				setattr(player,p_key,p_val)  

	packs = [] 
	milli_sec = getTime()
	for p_key,player in players.items(): 
		if checkStatus(player):
			continue
		if player.flagStarted: 
			if player.life>0: 
				setNewPlayerPosition(player)
				applyBullets(player) 
				checkBulletCollision(player)
		if not player.flagStarted: 
			_map = {'flagStarted':0}
			redisClient_0.hmset("p:"+player.userid,_map)
		pack = {}
		pack['name'] = player.player_name
		pack['userid'] = player.userid
		pack['lookAt'] = player.lookAt.toArray()
		pack['position'] = player.position.toArray()
		pack['bullets'] = getPlayerBullets(player)
		pack['bullets_size'] = len(player.bullets)
		pack['created_at'] = player.created_at
		pack['updated_at'] = player.updated_at
		pack['flagKill'] = 1 if player.flagKill else 0
		pack['flagStarted'] = 1 if player.flagStarted else 0
		pack['life'] = player.life
		packs.append(pack) 
		#print(pack)
	redisClient_1.set('gameValues',json.dumps(packs))
	wait = False
#render() 
def checkStatus(player): 
	global players
	global milli_sec
	global redisClient_0
	if player.flagStarted:
		if player.life<=0:
			player.life = Player.life_def
	if player.updated_at!="":
		diff_millis = milli_sec - int(player.updated_at) 
		if player.flagKill or diff_millis>10*1000: 
			if player.userid in players:
				del players[player.userid]
				redisClient_0.delete("p:"+player.userid)
				redisClient_0.srem("players",player.userid)
				return True
	return False 

def checkBulletCollision(player):
	global bullets
	for b_key, bullet in bullets.items():
		if bullet.visible:
			if bullet.player_id!=player.userid:
				d = player.position.distanceTo(bullet.position)
				if d<=(bullet.radius + player.radius):
					bullet.visible = False  
					player.decrementLife()

def checkPlayerOutside(player): 
	if player.position.x>70:
		player.position.x=70 

	if player.position.x < -70:
		player.position.x= -70

	if player.position.y>70:
		player.position.y=70

	if player.position.y<-70:
		player.position.y=-70

def setNewPlayerPosition(player):  
	player.vUnit.copy(player.lookAt).sub(player.position).normalize()
	distance = player.lookAt.distanceTo(player.position)
	distance = max(min(distance,10),0)
	if distance > 0.1:
		player.velocity = 0.1 if player.flagVelocity else 0.05
		if player.flagBackward:
			player.velocity *=-1
	else:
		player.velocity = 0
	player.position.addScaledVector(player.vUnit,player.velocity)
	checkPlayerOutside(player)

def newBullet():
	global bullets
	find = False
	for b_key,bullet in bullets.items():
		if not bullet.visible:
			find = True
			break
	if not find:
		bullet = Bullet()
		bullet.id = uuid.uuid1().hex
		bullets[bullet.id] = bullet
	return bullet

def createNewBullet(player):
	global players
	bullet_exists = False
	for b_key,bullet in player.bullets.items():
		if not bullet.visible:
			bullet_exists = True
			break
		
	if not bullet_exists: 
		bullet = newBullet()
		if bullet.player_id != player.userid:
			if bullet.player_id in players:
				another_player_bullets = players[bullet.player_id].bullets
				if bullet.id in another_player_bullets:
					another_player_bullets.pop(bullet.id) 
		player.bullets[bullet.id] = bullet 
		bullet.player_id = player.userid
 
	bullet.visible = True
	bullet.position.copy(player.position)
	bullet.direction.copy(player.vUnit) 
	bullet.velocity = 0.35  

def checkBulletOutside(bullet):
	if(bullet.position.x>100 or bullet.position.x<-100):
		bullet.visible = False
	if(bullet.position.y>100 or bullet.position.y<-100):
		bullet.visible = False

def applyBullets(player): 
	if player.interval_newBullet>0:
		player.interval_newBullet -= 1
	else:  
		if player.flagShoot:
			createNewBullet(player)
		player.interval_newBullet = player.interval_newBullet_def

def getPlayerBullets(player):
	arr = []
	for b_key,bullet in player.bullets.items():
		if bullet.visible:
			bullet.position.addScaledVector(bullet.direction,bullet.velocity)
			bullet.lookAt.copy(bullet.position).addScaledVector(bullet.direction,1)
			checkBulletOutside(bullet)
		if bullet.visible:
			json = {}
			json['position'] 	= bullet.position.toArray()
			json['direction'] 	= bullet.direction.toArray()
			json['lookAt'] 		= bullet.lookAt.toArray()
			json['id'] 			= bullet.id
			arr.append(json)

	return arr

def set_interval(func, sec):
    def func_wrapper():
        set_interval(func, sec)
        if not wait:
        	func()
    t = threading.Timer(sec, func_wrapper)
    t.start()
    return t


restore()
if not debug:
	set_interval(render,fps)
else: 
	print("-------------------")
	render()
#print fps		
