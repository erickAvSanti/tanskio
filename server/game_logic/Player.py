from Vector3 import Vector3

class Player:
	life_def = 100
	def __init__(self):
		self.userid = ''
		self.interval_newBullet = 10
		self.interval_newBullet_def = 10
		self.flagShoot = False
		self.player_name = ''
		self.flagVelocity = False
		self.flagBackward = False
		self.flagKill = False
		self.mvFB = False
		self.mvRL = False
		self.created_at = 0
		self.updated_at = 0
		self.position = Vector3()
		self.lookAt = Vector3()
		self.vUnit = Vector3()
		self.velocity = 0 
		self.bullets = {}
		self.life = Player.life_def
		self.flagStarted = False


		#once reading execution
		self.position_once = True
		self.position_once_flag = 0
		self.interval_newBullet_once = True
		self.interval_newBullet_once_flag = 0
		self.resurrect = False
		self.radius = 1
	def decrementLife(self):
		if self.life>0:
			self.life -= 2
			if self.life<0:
				self.life = 0
			if self.life<=0:
				self.flagStarted = False
	def __str__(self): 
		return self.toString() 
	def toString(self):
		keys = [a for a in dir(self) if not a.startswith('__')]
		_str = "" 
		for key in keys:
			val = getattr(self,key) 
			if isinstance(val,Vector3):
				_str+=key+"=["+val.toString()+"],"
			else:
				if type(val) is bool:
					_str+=key+"="+str(val)+"," 
				if type(val) is str:
					_str+=key+"="+str(val)+"," 
				if type(val) is dict:
					_str+=key+"="+repr(val)+","
				if type(val) is unicode:
					_str+=key+"="+val.encode('utf-8')+"," 
		return _str