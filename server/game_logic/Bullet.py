from Vector3 import Vector3

class Bullet:
	def __init__(self):
		self.id=''
		self.visible = True
		self.position = Vector3()
		self.direction = Vector3()
		self.lookAt = Vector3()
		self.velocity = 0
		self.player_id = '' 
		self.radius = 0.5
		'''
		type: int
		#1 => tipo de bala 1
		#2 => tipo de bala 2
		#3 => tipo de bala 3
		'''
		self.type=1
	def __str__(self):
		#return self.userid
		return self.toString()
	def toString(self):
		keys = [a for a in dir(self) if not a.startswith('__')]
		_str = "" 
		for key in keys:
			val = getattr(self,key) 
			if isinstance(val,Vector3):
				_str+=key+"=["+val.toString()+"],"
			else:
				if type(val) is str:
					_str+=key+"="+str(val)+"," 
				if type(val) is dict:
					_str+=key+"="+repr(val)+","
				if type(val) is unicode:
					_str+=key+"="+val.encode('utf-8')+"," 
		return _str