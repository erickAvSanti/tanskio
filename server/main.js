  
    var cluster = require('cluster');
    var numCPUs = require('os').cpus().length;
    console.log("#cpus: "+numCPUs);  



    if(cluster.isMaster){
        console.log("cluster is master");
        for(var _idx=0;_idx<numCPUs;_idx++){
            cluster.fork();
        }
        cluster.on('exit', (worker, code, signal) => {
            console.log("worker:",worker);
            console.log("code:",code);
            console.log("signal:",signal);
            cluster.fork();
        });
    }else{
        var gameport = 50001;  
        var UUID    = require('node-uuid');
        var THREE = require("three-js")(); 

        var ws = require("nodejs-websocket");
        var fs = require('fs');
        var _key_file = "../../../../../../etc/ssl/ssl.key/arquigamespe.key";
        var _crt_file = "../../../../../../etc/ssl/ssl.crt/arquigames_pe.crt";
        var redis = require("redis");
        var redisClient_0 = null;
        var redisClient_1 = null;
        if (
            fs.existsSync(_key_file) && 
            fs.existsSync(_crt_file)
        ) { 
            var options = {
                secure: true,
                key: fs.readFileSync(_key_file),
                cert: fs.readFileSync(_crt_file)
            }
            redisClient_0 = redis.createClient({host:"127.0.0.1",port:"6379"});
            redisClient_1 = redis.createClient({host:"127.0.0.1",port:"6379",db:1}); 
        }else{
            var options = {secure:false};
            redisClient_0 = redis.createClient({host:"172.17.0.2",port:"6379"});
            redisClient_1 = redis.createClient({host:"172.17.0.2",port:"6379",db:1});
        }
        redisClient_0.on("error", function (err) {
            console.log("Error redis Client 0: " + err);
        });
        redisClient_1.on("error", function (err) {
            console.log("Error redis Client 1: " + err);
        });   


        var _max_catch_exception = 50;
        var _count_catch_exception = 0;
        var _sockets = [];  
        function parseReply(conn,reply){
            try{
                var reply = JSON.parse(reply); 
                for(var idx in conn.props){
                    if(typeof conn.props[idx]=="number" && idx in reply){
                        conn.props[idx] = reply[idx];
                    }
                    if(typeof conn.props[idx]=="string" && idx in reply){
                        conn.props[idx] = reply[idx];
                    }
                    if(typeof conn.props[idx]=="boolean" && idx in reply){
                        conn.props[idx] = reply[idx];
                    }
                    if(typeof conn.props[idx]=="object" && conn.props[idx] instanceof THREE.Vector3 && idx in reply){
                        if(Array.isArray(reply[idx])){
                            conn.props[idx].x = reply[idx][0];
                            conn.props[idx].y = reply[idx][1];
                            conn.props[idx].z = reply[idx][2];
                        }else{
                            conn.props[idx].x = reply[idx].x;
                            conn.props[idx].y = reply[idx].y;
                            conn.props[idx].z = reply[idx].z;
                        }
                    }
                }
            }catch(e){}
        }
        function callback_hmset(err,res){
            if(err)console.log("ERROR: "+err);
        }
        function isFloat(val){
            return /^\-?(\d+\.\d+|\d+\.|\.\d+|\d+)$/.test(val);
        }
      
        var server = ws.createServer(options,function (conn) {
            _onConnection(conn); 
            conn.on("text", function (str) { 
                redisClient_0.hgetall("p:"+conn.props.userid,function(err,reply){//reply is json
                    if(err){
                        console.log(err);
                        return;
                    }
                    if(!reply){
                        _onConnection(conn);
                        return;
                    }
                    parseReply(conn,reply);
                    try{ 
                        var _json = JSON.parse(str);  
                        if(_json.name && typeof _json.name == "string"){
                            conn.props.player_name = _json.name;  
                        }
                        if(_json.onlookat===true){ 
                            if(
                                _json.msg && 
                                Array.isArray(_json.msg) && 
                                _json.msg.length>2 && 
                                isFloat(_json.msg[0]) && 
                                isFloat(_json.msg[1])
                            ){
                                conn.props.lookAt.x = _json.msg[0];
                                conn.props.lookAt.y = _json.msg[1];
                                conn.props.lookAt.z = 0; 
                            }  
                            if(typeof _json.shoot ==="boolean"){
                                conn.props.flagShoot = _json.shoot ? 1:0;
                            } 
                        }
                        if(_json.mvFB!==undefined && typeof _json.mvFB ==="number" && (_json.mvFB==1 || _json.mvFB==0)){
                            conn.props.mvFB = _json.mvFB;
                        }
                        if(_json.mvRL!==undefined && typeof _json.mvFB ==="number" && (_json.mvFB==1 || _json.mvFB==0)){
                            conn.props.mvRL = _json.mvRL;
                        } 
                        if(_json.v!==undefined && typeof _json.v ==="boolean"){
                            conn.props.flagVelocity = _json.v ? 1:0;
                        }
                        if(_json.r!==undefined && typeof _json.r ==="boolean"){
                            conn.props.flagBackward = _json.r ? 1:0;
                        }   
                        var _play = undefined;
                        if(_json.play!==undefined && typeof _json.play ==="boolean"){
                            _play = true;
                        }    
                        var _arr = [
                                "mvFB",conn.props.mvFB,
                                "mvRL",conn.props.mvRL,
                                "flagVelocity",conn.props.flagVelocity,
                                "flagBackward",conn.props.flagBackward,
                                "flagShoot",conn.props.flagShoot, 
                                "player_name",conn.props.player_name,
                                "updated_at",(new Date()).getTime(),
                                "lookAt",[
                                    conn.props.lookAt.x,
                                    conn.props.lookAt.y,
                                    conn.props.lookAt.z,
                                ].toString(),
                            ];
                        if(_play){
                            _arr.push("flagStarted",1);
                        }
                        
                        redisClient_0.hmset("p:"+conn.props.userid,_arr,callback_hmset);
                    }catch(e){
                        if(_count_catch_exception<_max_catch_exception){
                            console.log(e);
                            _count_catch_exception++;
                        } 
                    }
                });
            }) 
            conn.on("close", function (code, reason) {
                console.log("Connection closed",code,reason);
                console.log('\t socket disconnected ' + conn.props.userid ); 
                for(var _idx=0;_idx<_sockets.length;_idx++){
                    var _socket = _sockets[_idx];
                    if(_socket.userid == conn.props.userid){
                        _sockets.splice(_idx,1); 
                        break;
                    }
                } 
                redisClient_0.hgetall("p:"+conn.props.userid,function(err,reply){
                    if(reply){
                        redisClient_0.hmset("p:"+conn.props.userid,[
                            "flagKill",1,
                        ],callback_hmset);
                    }
                });
                
            });
            conn.on("error", function (err) {
                console.log("error: "+conn.props.userid,err); 
            });
        }).listen(gameport); 
        function _onConnection(socket){
            if(socket.props){
                var _find_s = false;
                for(var idz in _sockets){
                    var _s = _sockets[idz];
                    if(
                        _s.props.userid==socket.props.userid
                    ){
                        _find_s = true;
                        break;
                    }
                }
                if(!_find_s){
                    _sockets.push(socket); 
                    console.log("reconnecting with socket");
                }
            }else{
                _sockets.push(socket); 
            }
            setProperties(socket);
            var _msg = { 
                    onconnected:true,
                    msg:{
                        userid: socket.props.userid,
                        position:[
                            socket.props.position.x,
                            socket.props.position.y,
                            0
                        ] ,
                        lookAt:[
                            socket.props.lookAt.x,
                            socket.props.lookAt.y,
                            0
                        ],
                        bullets:[] 
                    }
                };
            var _json = JSON.stringify(_msg); 
            socket.send(
                _json
            ); 
            console.log('\t socket.io:: player ' + socket.props.userid + ' connected, at pid:'+process.pid);  
        } 
        function setProperties(socket){
            var _posX =THREE.Math.randFloat(-10,10);
            var _posY =THREE.Math.randFloat(-10,10);
            socket.props = {
                userid:UUID(),  
                flagShoot:0,
                flagBackward:0,
                player_name:'',
                flagVelocity:0,
                mvFB:0,
                mvRL:0, 
                created_at:(new Date()).getTime(),
                updated_at:(new Date()).getTime(),
            }; 
            socket.props.player_name = "unnamed";
            socket.props.position = new THREE.Vector3(_posX,_posY,0);
            socket.props.lookAt = new THREE.Vector3(_posX,_posY-1,0);  
            console.log("setting properties for: "+socket.props.userid);
            var _arr = [];
            for(idx in socket.props){
                var _val = socket.props[idx];
                if(typeof _val !=="object"){
                    _arr.push(idx,_val);
                }else{
                    if(_val instanceof THREE.Vector3){
                        _arr.push(idx,[_val.x,_val.y,_val.z].toString()); 
                    }
                }
            } 

            redisClient_0.hmset(
                "p:"+socket.props.userid,
                _arr,
                callback_hmset
                );
            redisClient_0.sadd(
                "players",
                socket.props.userid,
                function(err,res){
                    console.log(err,res);
                });
        };  
        
        var _fps = function(){  
 
            var _packs = []; 

            redisClient_1.get('gameValues',function(err,reply){
                if(err)console.log(err);
                if(reply){
                    try{
                        _packs = JSON.parse(reply);
                        if(_packs.length>0){
                            _packs = {onclients:true,msg:_packs};
                            for(var _idx=0;_idx<_sockets.length;_idx++){
                                _socket = _sockets[_idx];
                                if(_socket.readyState == _socket.OPEN){
                                    _socket.sendText(JSON.stringify(_packs));
                                }
                                
                            } 
                            delete _packs;
                        } 
                    }catch(e){
                        console.log(e);
                    } 
                }
            });

            
        };
        var _interval = setInterval(_fps,1000/60);
        console.log("Worker "+process.pid+" started");
    } 
    