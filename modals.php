<div id="popup">
	<div class="cc">
		<div class="container"> 
			<div id="pp1" style="display:none;">
				<div style="width:100%;text-align:center;">
					<h4 class="cFont2" class="modal_title">Multiplayer Dev</h4>
					<label class="modal_desc">
						This is a unofficial indie game in dev mode.
					</label>
					<b><i><label class="game_status"></label></i></b>
				</div>
				<div class="input-group mb-3">
					<div class="input-group-prepend">
						<!-- 
					    <span class="input-group-text" id="made_title" title="Made in Peru">🇵🇪</span>
 						-->
 						<div class="btn btn-info fa fa-cogs" id="conf"> 
 						</div>
				 	</div>
				 	<input type="text" class="form-control" id="nickname" placeholder="nickname" aria-label="nickname" aria-describedby="nickname">
				  	<div class="input-group-append">
				    	<div class="btn btn-g-play cFont2" id="play">
		  					Play
		  				</div>
				  	</div>
				</div> 
				<ul>
					<li>
						r: retro
					</li>
					<li>
						v: velocity
					</li>
					<li>
						mouse left: press [attack]
					</li>
				</ul> 
			</div>
			<div id="pp2" style="display:none;text-align: center;">
				<div style="width:100%;text-align:center;">
					<h4 class="cFont2 modal_title">Multiplayer Dev</h4>
					<label class="modal_desc">
						This is a unofficial indie game in dev mode.
					</label>
				</div>
				<i class="fa fa-warning" style="font-size: 30px;color: orange;"></i>
				<label>Mantiene una ventana abierta</label>
			</div>
			<div id="pp3">
				<div style="width:100%;text-align:center;">
					<h4 class="cFont2" class="modal_title">Multiplayer Dev</h4>
					<label class="modal_desc">
						This is a unofficial indie game in dev mode.
					</label>
				</div>
				<div style="width:100%;text-align:center;">
					<img src="./imgs/giphy_arrows.gif"/>
					<label class="modal_connecting">Conectando...</label>
				</div> 
			</div>
		</div> 
	</div>
	<div class="cc2"> 
		<div style="display: 100%;text-align: center;">
			<label class="modal_ad">Anuncio</label>	
		</div>
		<div id="ad-aq">
			<ins class="adsbygoogle"
		     	style="display:inline-block;width:300px;height:250px"
		     	data-ad-client="ca-pub-6209186190244737"
		     	data-ad-slot="7952631853"></ins>   
		</div> 
	</div>
</div>


<div class="modals"> 
	<div id="modal_lost" class="modal fade" role="dialog" data-backdrop="static" data-keyboard="false">
	  	<div class="modal-dialog modal-md"> 
	    	<!-- Modal content-->
	    	<div class="modal-content">
	      		<div class="modal-header"> 
	        		<h4 class="modal-title cFont cColor1">Balls IO</h4>
	      		</div>
	      		<div class="modal-body">  
	      		</div>
	      		<div class="modal-footer"> 
	      		</div>
	    	</div> 
	  	</div>
	</div>
</div>  

<div id="modal_conf" class="modal animated fadeIn fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="container">
        	<ul class="nav nav-tabs" id="myTab" role="tablist">
			  <li class="nav-item">
			    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
			    	<i class="fa fa-rocket"></i>
			    </a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Profile</a>
			  </li>
			  <li class="nav-item">
			    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Contact</a>
			  </li>
			</ul>
			<div class="tab-content" id="myTabContent">
			  <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">...</div>
			  <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">...</div>
			  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">...</div>
			</div>
        </div>
      </div> 
    </div>
  </div>
</div>