$(window).ready(
	function(evt){  
		setCanvasDim();
		$(window).resize(
			function(evt){
				setCanvasDim();
			}
		);
		(window.appMain = appgame.Main).start();
		Math.prototype.clamp = function(num, min, max) {
		  	return num <= min ? min : num >= max ? max : num;
		};
	}
); 
function setCanvasDim(){
	window.w_width = window.innerWidth;
	window.w_height = window.innerHeight;
	$("canvas").each(
		function(index,obj){
			var _this = $(obj);
			var _id = _this.attr("id");
			if(
				_id!="webgl" &&  
				_id!="canvas-assets" &&  
				_id!="canvas-touch" 
			){
				return;
			}
			_this.css("width",w_width+"px").css("height",w_height+"px");
			_this.attr("width",w_width).attr("height",w_height);
		}
	);
}

var appgame = appgame || {};
appgame.Main = (
	function(){

		var _instance = null;

		var _router = null;
		var _zona_a = null;
		var _ambientLight = new THREE.AmbientLight( 0xffffff); 
		var _directionalLight = new THREE.DirectionalLight( 0xaaaaaa, 0.1 ); 

		 

		var _cameraZPosInit = 10;
		var _cameraYPosInit = 0;
		var _cameraYPosInit_min = _cameraYPosInit;
		var _cameraXPosInit = 0;

		var _cameraAngle_def = 60;
		var _cameraAngle = _cameraAngle_def;

		var _fullScreen = null; 
		var _leaderBoard = null; 
		var _realTime = null; 
		var _assets = null; 
		var _enemies = null; 
		var _stats = null;

		_debug_touch = true;

		window._lib_game_finish = false;

		var _mesh = null;
		var _mesh_added = false;

		var _key_browser = "bkey";
		var _key_browser_time = "btime";
		var _interval_key_browser = null;

		var lang = {

			es:{
				title:"Multijugador",
				modal_title:"Multijugador",
				made_title:"Hecho en Perú",
				nickname:"Apodo",
				desc:"Este es un juego indie no oficial en modo desarrollo.",
				btn_play:"Jugar",
				tab_opened:"Mantiene una pestaña abierta",
				ad:"Anuncio",
				connecting:"Conectando...",
				settings:"Configuración",
			},
			en:{ 
				title:"Multiplayer Dev",
				modal_title:"Multiplayer Dev",
				made_title:"Made in Peru",
				nickname:"nickname",
				desc:"This is a unofficial indie game in dev mode.", 
				btn_play:"Play",
				tab_opened:"Keep an open tab",
				ad:"Advertisement",
				connecting:"Connecting...",
				settings:"Settings",
			},
			br:{
				popup:{
					title:"Hello",
				},
			}, 
		};

		var _curr_language = lang["en"];

		Main = function(){ 

			window.w_wgl 		= document.getElementById("webgl");
 			window.w_cassets 	= document.getElementById("canvas-assets");
 			window.w_ctouch 	= document.getElementById("canvas-touch");
 			if(window.w_cassets && window.w_cassets.getContext)window.w_cassets_ctx = window.w_cassets.getContext("2d");
 			if(window.w_ctouch && window.w_ctouch.getContext)window.w_ctouch_ctx = window.w_ctouch.getContext("2d");

		};
		Main.prototype = {
			constructor:Main,
			start:function(){

				var _lang = navigator.language || navigator.userLanguage || "en-US";
				if(_lang){
					_lang = _lang.split("-");
					if(Array.isArray(_lang) && _lang.length==2){
						_lang = _lang[0];
						if(_lang=="es" || _lang=="en"/* || _lang=="br"*/){
							_curr_language = lang[_lang]; 
						}
					}
				}
				$(".modal_title").html(_curr_language.title);
				$(".modal_desc").html(_curr_language.desc);
				$(".modal_ad").html(_curr_language.ad);
				$(".modal_connecting").html(_curr_language.connecting);
				$("#made_title").attr("title",_curr_language.made_title);
				$("#nickname").attr("placeholder",_curr_language.nickname);
				$("#play").html(_curr_language.btn_play);
				$("#pp2>label").html(_curr_language.tab_opened);
				console.log("current language: "+_lang);
				w_wgl._lang = _lang;

				if(!localStorage)return;
				var _it = localStorage.getItem(_key_browser);
				var _it_time = localStorage.getItem(_key_browser_time);
				var curr_time = (new Date()).getTime();
				if(!_it || (curr_time - _it_time)>60000){
					localStorage.setItem(_key_browser,Math.random());
					localStorage.setItem(_key_browser_time,(new Date()).getTime());
					_interval_key_browser = setInterval(function(){
						var _it = localStorage.getItem(_key_browser);
						if(!_it){
							localStorage.setItem(_key_browser,Math.random());
							localStorage.setItem(_key_browser_time,(new Date()).getTime());
						} 
					},500);
					$(window).on('beforeunload', function() {
						if(_interval_key_browser)clearInterval(_interval_key_browser);
						localStorage.removeItem(_key_browser);
						localStorage.removeItem(_key_browser_time);
					});
				}else{
					console.log("existe ota pestaña abierta.");
					$("#pp1").hide();
					$("#pp2").show();
					return;
				}
				$("#modal_conf .modal-title").html(_curr_language.settings);
				$("#conf").click(function(evt){
					$("#modal_conf").modal("show");
				});


				window._lib_lvl = 1;
				var scene = new THREE.Scene();
				scene.background = new THREE.Color(0.5,0.5,0.9);
				var camera = new THREE.PerspectiveCamera( _cameraAngle, w_width / w_height, 0.1, 1000 );

				var renderer = new THREE.WebGLRenderer({canvas:w_wgl,antialias:true}); 
				renderer.setSize(w_width,w_height);
				renderer.setPixelRatio(window.devicePixelRatio);
    	
  				_directionalLight.position.set(0,0,10); 
  				_directionalLight.target = new THREE.Object3D();
  				_directionalLight.target.position.set(0,0,0);


				scene.add( _ambientLight );
				scene.add( _directionalLight ); 
				scene.add( _directionalLight.target ); 
				camera.position.set(_cameraXPosInit,_cameraYPosInit,_cameraZPosInit);
				camera.lookAt(new THREE.Vector3());

				w_wgl.scene = scene;
				w_wgl.camera = camera;
				w_wgl.renderer = renderer; 
				w_wgl.playing = false;
				w_wgl.rqsAnim = true;

				w_wgl._lib_currTime = Date.now();
				w_wgl._lib_lastTime = w_wgl._lib_currTime;

				w_wgl.popup_visible = true;
 

				var _this = this;
				$(window).resize(
					function(evt){
						_this.resize();
					}
				);
				$(window).focus(
					function(evt){
						_this.focus();
					}
				);
				$(window).blur(
					function(evt){ 
						_this.blur();
					}
				);

				w_ctouch.addEventListener("touchstart",_this.touchstart,false);
				w_ctouch.addEventListener("touchend",_this.touchend,false);
				w_ctouch.addEventListener("touchcancel",_this.touchcancel,false);
				w_ctouch.addEventListener("touchleave",_this.touchleave,false);
				w_ctouch.addEventListener("touchmove",_this.touchmove,false);

				_stats = new Stats();
				_stats.showPanel( 0 );
				document.body.appendChild( _stats.dom );

				_this.animate();
 
				(_fullScreen = appgame.FullScreen.instance()).start();  
				(_leaderBoard = appgame.LeaderBoard.instance()).start();  
				(_realTime = appgame.Realtime.instance()).start();  
				(_assets = appgame.Assets.instance()).start();  

				_this.showPresentation();
				_this.procesarNivelJuego();
 
				_this.parseVendor();
				$("#play").click(function(_evt){ 
					_instance.popupStatus("hide"); 
				}); 
				$("#settings").click(function(_evt){
					_instance.popupStatus("show");
				}); 
			  	$(window).keyup(function(_evt){
			  		//console.log("keyup",_evt.key);
			  		if(_evt.key=="Escape"){
			  			_instance.popupStatus("show");
			  		}
			  	});

			}, 
			popupStatus:function(st){
				if(st=="show"){
					$("#popup").show();
					w_wgl.popup_visible = true;
				}
				if(st=="hide"){
					$("#popup").hide();
					w_wgl.popup_visible = false;
				}
			},
			touchstart:function(_evt){  
			},
			touchend:function(_evt){  
			},
			touchcancel:function(_evt){  
			},
			touchleave:function(_evt){  
			},
			touchmove:function(_evt){  
			}, 
			animate:function(){
				if(w_wgl.rqsAnim){
					w_wgl.rqs_anim_id = requestAnimationFrame( _instance.animate );
				}  
				_stats.begin();
				if(!w_wgl._lib_currTime){
					w_wgl._lib_currTime = Date.now();
					w_wgl._lib_lastTime = w_wgl._lib_currTime; 
				}
				w_wgl._lib_lastTime = w_wgl._lib_currTime;
				w_wgl._lib_currTime = Date.now();

				if(w_ctouch_ctx)w_ctouch_ctx.clearRect(0,0,w_width,w_height);
				if(_fullScreen)_fullScreen.render(); 
				if(_leaderBoard)_leaderBoard.render(); 
				if(_assets)_assets.render();  
				if(_assets && !_mesh){
					_mesh = _assets.getNave();
					if(_mesh){
						_mesh.name_mesh = _assets.getPlayerNameMesh("unnamed"); 
					}
				}
				if(_mesh && !_mesh_added){
					_mesh_added = true;
					w_wgl.scene.add(_mesh);
					w_wgl.scene.add(_mesh.name_mesh);
				}
				if(_realTime)_realTime.render(_mesh); 
				w_wgl.renderer.render(w_wgl.scene, w_wgl.camera);
				_stats.end();
			},
			blur:function(){
				//this.stopAnimation();
			},
			focus:function(){
				this.playAnimation();
			}, 
			stopAnimation:function(){
				w_wgl.rqsAnim = false;
				if(w_wgl.rqs_anim_id){
					cancelAnimationFrame(w_wgl.rqs_anim_id);
				}
			},
			playAnimation:function(){
				if(w_wgl.rqs_anim_id)cancelAnimationFrame(w_wgl.rqs_anim_id);
				w_wgl.rqsAnim = true;
				this.animate();
			},
			resize:function(){
				w_wgl.camera.aspect = w_width / w_height;
    			w_wgl.camera.updateProjectionMatrix();
				w_wgl.renderer.setSize(w_width,w_height);
				if(_fullScreen)_fullScreen.resize();  
			},
			parseVendor:function(){
				if(!window._lib_vendor && w_wgl.renderer){
					var ctx = w_wgl.renderer.getContext();
					var info = w_wgl.renderer.extensions.get("WEBGL_debug_renderer_info");
					window._lib_vendor = {};
					window._lib_vendor.vendor 		= ctx.getParameter(ctx.VENDOR);
					window._lib_vendor.renderer 	= ctx.getParameter(ctx.RENDERER);
					window._lib_vendor.u_vendor 	= ctx.getParameter(info.UNMASKED_VENDOR_WEBGL);
					window._lib_vendor.u_renderer 	= ctx.getParameter(info.UNMASKED_RENDERER_WEBGL);

					$("#cpu_info1").html(_lib_vendor.vendor);
					$("#cpu_info2").html(_lib_vendor.renderer);
					$("#cpu_info3").html(_lib_vendor.u_vendor);
					$("#cpu_info4").html(_lib_vendor.u_renderer); 
					$("#cpu_info5").html("#Procesadores: "+navigator.hardwareConcurrency); 

				}
			},
			showPresentation:function(){  

				window._lib_game_paused = true; 
			},
			procesarNivelJuego:function(){ 
				this.cargarZonaA();
			},
			cancelarCargaActual:function(){
				this.eliminarObjetosDeEscena();
			},
			eliminarObjetosDeEscena:function(){
				if(w_wgl.scene){ 
				} 
			},
			cargarZonaA:function(){
				if(_zona_a)_zona_a.cargarElementos();
			}
		};

		return {
			start:function(){
				if(!_instance){
					_instance = new Main();
					_instance.start();
				}
			}
		};
	}
)();
