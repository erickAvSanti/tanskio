var appgame = appgame || {};

appgame.Realtime = (
	function(){

		var _instance = null;  
		var _socket = null;
		var _data = null;
		var _mousePos = new THREE.Vector2();
		var _mousePosLast = new THREE.Vector2();
		var _mouse3DPos = new THREE.Vector3();
		var _enemies = null;
		var _userid = null;
		var _assets = null;
		var _leaderboard = null;

		var _moveFrontBack = 0;//1: front, -1 : back
		var _moveLeftRight = 0;//1: left, -1 : right

		var _keyW = false;
		var _keyS = false;
		var _keyA = false;
		var _keyD = false;
		var _keyV = false;
		var _keyR = false;

		var _shoot = false;

		var _gameStatus = 0;

		var _dataToSendServer = function(){
			if(!w_wgl.camera)return;
			_mouse3DPos.set(
			    (_mousePos.x / window.innerWidth) * 2 - 1,
			    - (_mousePos.y / window.innerHeight) * 2 + 1,
			    0.5
			);
			_mouse3DPos.unproject(w_wgl.camera); 
			_mouse3DPos.sub(w_wgl.camera.position); 
			_mouse3DPos.normalize(); 
			_moveFrontBack = 0;
			_moveLeftRight = 0;
			if(_keyW)_moveFrontBack+=1;
			if(_keyS)_moveFrontBack-=1;
			if(_keyA)_moveLeftRight+=1;
			if(_keyD)_moveLeftRight-=1;

			t = (-1)*w_wgl.camera.position.z/_mouse3DPos.z; 
			x = w_wgl.camera.position.x + t*_mouse3DPos.x;
			z = w_wgl.camera.position.z + t*_mouse3DPos.z;
			y = w_wgl.camera.position.y + t*_mouse3DPos.y; 
			_mouse3DPos.set(x,y,z); 
			if(_mousePosLast.x!=_mousePos.x && _mousePosLast.y!=_mousePos.y){ 
				var _msg = {
					onlookat:true,
					msg:[_mouse3DPos.x,_mouse3DPos.y,_mouse3DPos.z],
					mvFB:_moveFrontBack,
					mvLR:_moveLeftRight,
					v:_keyV,
					r:_keyR,
					shoot:_shoot,
				}; 
				socketSend(_msg,true);
				
			} 
		};
		function socketSend(data,_json){
			if(_socket && _socket.readyState == _socket.OPEN){
				_socket.send(_json ? JSON.stringify(data) : data);
			}
		}


		Realtime = function(){

		};
		Realtime.prototype = {
			constructor:Realtime, 
			test:function(){
				console.log("hello from Realtime object");
			},
			onConnected:function(data){ 
				_userid = data.userid; 
				_data = data;
				console.log(data);
		  		console.log( 'Connected successfully to the socket.io server. My server side ID is ' + data.userid );
				$("#popup").show();
				w_wgl.popup_visible = true;
				$("#pp1").show();
				$("#pp3").hide();
			},
			start:function(onlySocket){ 
				if(!onlySocket){
					(_enemies = appgame.Enemies.instance(this)).start();  
					(_leaderboard = appgame.LeaderBoard.instance()).start();  
					_assets = appgame.Assets.instance(); 
				}
				if(window.location.protocol=="http:"){
					_socket = new WebSocket('ws://localhost:50001');
				}else{
					_socket = new WebSocket('wss://arquigames.pe:50001');
				} 
				_socket.onopen = function(_evt){ 
					console.log("open",_evt);
					_assets.showElements();
					$("#pp1").show();
					$("#pp3").hide();
					$("#popup").show();
					w_wgl.popup_visible = true;
				};
				_socket.onerror = function(_evt){ 
					console.log("error",_evt);
					_assets.hideElements();
					$("#pp1").hide();
					$("#pp3").show();
					$("#popup").show();
					w_wgl.popup_visible = true;
				};
				_socket.onmessage = function(_evt){ 
					try{
						var _res =JSON.parse(_evt.data);
						if(_res.onconnected){
							_instance.onConnected(_res.msg);
						}
						if(_res.onclients){
							_instance.parseClients(_res.msg);
						}
					}catch(exc){

					}
				};
				_socket.onclose = function(_evt){
					console.log("close",_evt);
					_assets.hideElements();
					$("#popup").show();
					$("#pp1").hide();
					$("#pp3").show();
					w_wgl.popup_visible = true;
					setTimeout(function(){_instance.start(true)},5000);
				}; 
			  	if(!onlySocket){
				  	$(window).keypress(function(_evt){
				  		//console.log("keypress",_evt.key); 
				  		if(_evt.key=="w")_keyW = true;
				 		if(_evt.key=="s")_keyS = true;
				  		if(_evt.key=="a")_keyA = true;
				 		if(_evt.key=="d")_keyD = true;
				 		if(_evt.key=="v")_keyV = true;
				 		if(_evt.key=="r")_keyR = true;
				  	});
				  	$(window).keyup(function(_evt){
				  		//console.log("keyup",_evt.key);
				  		if(_evt.key=="w")_keyW = false;
				 		if(_evt.key=="s")_keyS = false;
				  		if(_evt.key=="a")_keyA = false;
				 		if(_evt.key=="d")_keyD = false;
				 		if(_evt.key=="v")_keyV = false;
				 		if(_evt.key=="r")_keyR = false;
				  	});
				  	$(window).mousemove(function(_evt){
				  		_mousePos.set(_evt.clientX,_evt.clientY);
				  	}); 
				  	$(window).mousedown(function(_evt){
				  		//console.log(_evt);
				  		if(_evt.which==1){ 
				  			if(!w_wgl.popup_visible){
				  				_shoot = true;
				  				//console.log("emiting shooting");
				  			}
				  		}
				  	}); 
				  	$(window).mouseup(function(_evt){ 
				  		if(_evt.which==1){ 
				  			_shoot = false;
				  		}
				  	}); 
					$("#nickname").keyup(function(evt){ 
						evt.preventDefault();
						evt.stopPropagation();
						var _obj = $(this);
						if(evt.which==13){
							$("#popup").hide();
							w_wgl.popup_visible = false;
							socketSend({name:_obj.val(),play:true},true);
						}
					}); 
					$("#play").click(function(_evt){  
						socketSend({name:$("#nickname").val(),play:true},true);
					});
				  	setInterval(_dataToSendServer,1000/60);
			  	}
			}, 
			parseClients:function(data){ 
				if(_leaderboard){ 
					_leaderboard.setData(data); 
				}
				if(_enemies){
					_enemies.parseClients(data,_userid);
				}
			},
			setData:function(data){
				_data = data;
			},
			render:function(_mesh){
				if(_data){
					if(_data.life<=0){
						if(_gameStatus==0)_gameStatus = 1;
					}else{
						_gameStatus = 0;
					}
				}
				if(_gameStatus==1){
					//mostrar popup de perdida
					$(".game_status").html("Perdiste");
					$("#popup").show();
					w_wgl.popup_visible = true;
					_gameStatus = 2;
				}
				if(_data && _mesh && _data.flagStarted){ 
					_mesh.userid = _userid; 
					_mesh.position.fromArray(_data.position);
					_mesh.visible = true;  
					w_wgl.camera.position.x =_mesh.position.x;
					w_wgl.camera.position.y =_mesh.position.y; 
					w_wgl.camera.lookAt(_mesh.position); 
					_mesh.lookAt(_mouse3DPos); 
					if(_mesh.name_mesh){
						if(_assets && _data.name!=_mesh.name_mesh.text && typeof _data.name =="string"){
					 		_mesh.name_mesh.text = _mesh.name_mesh.material.map.text = _data.name; 
						}
						_mesh.name_mesh.position.copy(_mesh.position);
						_mesh.name_mesh.visible = true;
					}
					if(!_mesh.life_mesh){
						_assets.setLife(_mesh);
						if(_mesh.life_mesh){ 
							_mesh.life = _data.life;
							w_wgl.scene.add(_mesh.life_mesh);  
						}
					}else{
						_mesh.life_mesh.visible = true;
					}
					if(_mesh.life_mesh && _assets){  
						_mesh.life = _data.life;
						_mesh.life_mesh.position.copy(_mesh.position);
						_assets.drawLife(_mesh);
					}
				}
				if(_enemies)_enemies.render();  
			},
			animate:function(){ 
			}  
		};

		return {
			instance:function(){
				if(!_instance){
					_instance = new Realtime();
					return _instance;
				}
				return null;
			}
		};
	}
)();
