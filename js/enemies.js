var appgame = appgame || {};

appgame.Enemies = (
	function(){

		var _instance = null;   
		var _enemies = [];
		var _assets = null;
		var _parent = null;
		var _bullets = [];
		Enemies = function(p){
			_parent = p;
			_assets = appgame.Assets.instance();
		};
		Enemies.prototype = {
			constructor:Enemies, 
			test:function(){
				console.log("hello from Enemies object");
			},
			start:function(){  
			}, 
			render:function(){ 
			},
			animate:function(){ 
			},
			parseBullets:function(import_bullets){
				var _bullet = _assets.get("bullet");
				if(!_bullet)return;
				var _idx,_idy;
				for(_idx=0;_idx<_bullets.length;_idx++){
					_bullets[_idx].visible = true;
				}
				if(import_bullets.length>_bullets.length){ 
					var _diff = import_bullets.length - _bullets.length;
					for(_idx=0;_idx<_diff;_idx++){
						var _b = _bullet.clone();
						_b.up.set(0,0,1);
						_bullets.push(_b);
						w_wgl.scene.add(_b);
					}
				}else if(import_bullets.length<_bullets.length){
					var _diff = _bullets.length - import_bullets.length;
					for(_idx=0;_idx<_diff;_idx++){
						_bullets[_idx].visible = false;
					}

				} 
				_idy=0;
				for(_idx=0;_idx<_bullets.length;_idx++){
					var _obj = _bullets[_idx];
					if(_obj.visible){
						var _ib = import_bullets[_idy];
						_obj.position.fromArray(_ib.position,0);
						if(_ib.lookAt){ 
							_obj.lookAt( 
								_ib.lookAt[0],
								_ib.lookAt[1],
								_ib.lookAt[2] 
							);
						}
						_idy++;
					}
				}
			},
			parseClients:function(_clients,_userid){ 
				var _import_bullets = [];
				var _client,_enemy;
				for(var _idx=0;_idx<_clients.length;_idx++){
					_client = _clients[_idx]; 
					if(_client.flagStarted && _client.bullets){
						for(var _idy=0;_idy<_client.bullets.length;_idy++){
							_import_bullets.push(_client.bullets[_idy]); 
						}
					}
					if(_userid!=_client.userid){
						if(!_client.flagStarted)continue; 
						var _find = false;
						_enemy = null;
						for(var _idy=0;_idy<_enemies.length;_idy++){
							_enemy = _enemies[_idy];
							if(_enemy.userid == _client.userid){
								_find = true;
								break;
							}
						}
						if(!_find){ 
							_enemy = _assets.getNave();
							if(_enemy){ 
								_enemy.userid = _client.userid;
								_enemies.push(_enemy);
								_enemy.name_mesh = _assets.getPlayerNameMesh(_client.name);
								w_wgl.scene.add(_enemy.name_mesh);
								w_wgl.scene.add(_enemy);
							}
						}
						if(_enemy){ 
							if( _client.name!=_enemy.name_mesh.text && typeof _client.name =="string"){ 
								_enemy.name_mesh.text = _enemy.name_mesh.material.map.text = _client.name;
							}
							_enemy.position.fromArray(_client.position,0);
							_enemy.name_mesh.position.copy(_enemy.position); 	
							_enemy.lookAt(
									_client.lookAt[0],
									_client.lookAt[1],
									_client.lookAt[2]
								); 

							if(!_enemy.life_mesh){
								_assets.setLife(_enemy);
								if(_enemy.life_mesh){ 
									_enemy.life = _client.life;
									w_wgl.scene.add(_enemy.life_mesh); 
								}
							}
							if(_enemy.life_mesh && _assets){  
								_enemy.life = _client.life;
								_enemy.life_mesh.position.copy(_enemy.position);
								_assets.drawLife(_enemy);
								_enemy.life_mesh.visible = true;
							}
							if(_enemy.name_mesh){
								_enemy.name_mesh.visible = true;
							}
						}
					}else{
						_parent.setData(_client);
					}
				}
				this.parseBullets(_import_bullets);
				//delete
				var _ids_to_deleted = [];
				for(var _idy=0;_idy<_enemies.length;_idy++){
					_enemy = _enemies[_idy];
					_find = false;
					for(var _dx=0;_dx<_clients.length;_dx++){
						_client = _clients[_dx];
						if(_client.userid==_enemy.userid && _client.flagStarted){
							_find = true;
							break;
						}
					}
					if(!_find){
						_ids_to_deleted.push(_idy);
					}
				} 
				for(var _idy=0;_idy<_ids_to_deleted.length;_idy++){  
					var _id = _ids_to_deleted[_idy];
					_enemy = _enemies[_id];
					if(_enemy.name_mesh){
						w_wgl.scene.remove(_enemy.name_mesh);
					}
					if(_enemy.life_mesh){
						w_wgl.scene.remove(_enemy.life_mesh);
					}
					w_wgl.scene.remove(_enemy);
					_enemies.splice(_id,1);
				} 
				delete _ids_to_deleted;
			}
		};

		return {
			instance:function(p){
				if(!_instance){
					_instance = new Enemies(p);
					return _instance;
				}
				return null;
			}
		};
	}
)();
