var appgame = appgame || {};

appgame.Assets = (
	function(){

		var Rot360g = 2*Math.PI;
		var Rot180g = Math.PI;
		var Rot1g = Math.PI/180;
		var Rot10g = Math.PI/18;
		var Rot20g = Math.PI/9;
		var Rot30g = Math.PI/6;

		var _instance = null;
		var _piso = null;  
		var _nave = null;
		var _assets = {} 
		var _loader = new THREE.JSONLoader();

		var _debug_ctype_text = true;

		function animFunc(obj){
			if(obj.step<obj.to){
				obj.step+=obj.inc;
			}else{
				obj.step = obj.from;
			}
		}
		function getFlippedAnimOpacity(obj){
			var _opacity2 = 0;
			if(obj.step<obj.flip){
				_opacity2 = obj.step/obj.flip; 
			}else{
				var _diff1 = obj.to-obj.flip;
				var _step2 = obj.to-obj.step;
				_opacity2 = _step2/_diff1;
			}
			return _opacity2;
		}

		function getRandomColor() {
		  	var letters = '0123456789ABCDEF';
		  	var color = '#';
		  	for (var i = 0; i < 6; i++) {
		    	color += letters[Math.floor(Math.random() * 16)];
		  	}
		  	return color;
		}

		function createTextCanvas(text, color, font, size) {
			size = size || 24;
			var canvas = document.createElement('canvas');
			var ctx = canvas.getContext('2d');
			var fontStr = (font || 'Arial') + ' ' + (size +'px');
			ctx.font = fontStr; 
			var w = ctx.measureText(text).width;
			var h = Math.ceil(size*1.25);
			canvas.width = w;
			canvas.height = h;
			ctx.font = fontStr;
			ctx.fillStyle = color || 'black';
			ctx.fillText(text, 0, size);
			return canvas;
		}
		function getCanvasTexture(canvas){
			var canvas_texture = new THREE.CanvasTexture(canvas);
			canvas_texture.playable = true;
			if(_debug_ctype_text){
				canvas_texture.ctype = "asset_canvas_texture";
			}
			return canvas_texture;
		}
		function getCanvasNaveTexture(canvas){
			if(!canvas){ 
				canvas = document.createElement('canvas');
			}
			var effect = function(ctx){ 
				
				//------------------------------------------
				ctx.clearRect(0,0,ctx.width,ctx.height);
				//------------------------------------------
				animFunc(ctx.custom.animation1);
				var _opacity2 =  getFlippedAnimOpacity(ctx.custom.animation1);
				var _opacity = ctx.custom.animation1.step/100.0; 
				var grd3=ctx.createRadialGradient(512,512,0,512,512,400);
				grd3.addColorStop(0,`rgb(0,0,0,0)`);
				grd3.addColorStop(1,`rgb(255,255,255,${_opacity2})`);
				//------------------------------------------
				ctx.beginPath();
				ctx.fillStyle=grd3;  
				ctx.arc(512,512,Math.round(400*_opacity),0,Rot360g);  
				ctx.fill();   
				//------------------------------------------
				ctx.beginPath();
				ctx.fillStyle=ctx.custom.gradient2; 
				ctx.lineWidth=0;
				ctx.arc(512,512,400,0,Rot360g);  
				ctx.fill();   
				//------------------------------------------
				ctx.beginPath();
				ctx.fillStyle=ctx.custom.gradient1; 
				ctx.lineWidth=0;
				ctx.arc(512,512,200,0,Rot360g);  
				ctx.fill();   
				//------------------------------------------
				ctx.beginPath();
				ctx.strokeStyle="#00000033";
				ctx.lineWidth=70;
				ctx.arc(512,512,400,0,Rot360g);  
				ctx.stroke(); 
				//------------------------------------------
				ctx.beginPath();
				ctx.strokeStyle="#FF0000FF";
				ctx.lineWidth=20;
				ctx.arc(512,512,400,0,Rot360g);  
				ctx.stroke(); 
				//------------------------------------------
			};
			//-----------------------------------------------------
			var ctx = canvas.getContext('2d');
			ctx.width = ctx.height = canvas.width = canvas.height = 1024; 
			//-----------------------------------------------------
			var canvas_texture = new THREE.CanvasTexture(canvas);
			canvas_texture._canvas = canvas;
			canvas_texture.playable = true;
			if(_debug_ctype_text){
				canvas_texture.ctype = "asset_canvas_nave_texture";
			}
			//-----------------------------------------------------
			var _func = function(){
				if(!ctx.custom){
					ctx.custom = {
						animation1:{
							from:0.0,
							to:100.0,
							step:0.0,
							inc:5.0,
							flip:70.0,
						}
					};
					var grd=ctx.createRadialGradient(512,512,2,512,512,400);
					grd.addColorStop(0,"#FFA50077");
					grd.addColorStop(1,"#FFFFFF33");
					var grd2=ctx.createRadialGradient(512,512,2,512,512,400);
					grd2.addColorStop(0,"#FFA500");
					grd2.addColorStop(1,"#00000000");
					ctx.custom.gradient1 = grd;
					ctx.custom.gradient2 = grd2; 
				} 
				effect(ctx);
				canvas_texture.needsUpdate = true;
				_func2();
			};
			var _func2 = function(){
				window.setTimeout(_func,100);
			};
			_func2();
			return canvas_texture;
		}
		function getCanvasNaveSprite(){
			var _map = getCanvasNaveTexture();
			var spriteMaterial = new THREE.SpriteMaterial( { map: _map, transparent:true } );
			var nave =  new THREE.Sprite(spriteMaterial);
			nave.scale.set(2,2);
			nave._material = spriteMaterial;
			nave._map = _map;
			console.log(nave); 
			return nave;
		}

		function getTextSprite(text,size){
			var texture_sprite =  new THREE.TextSprite({
			  	textSize: size, 
			  	redrawInterval: 250, 
			  	texture: {
			    	text: text,
			    	//fontFamily: 'Arial, Helvetica, sans-serif',
			    	fontFamily: 'Tahoma, Geneva, sans-serif',
			    	fontWeight: 'bold', 
			    	lineWidth: 0.1, 
			    	strokeStyle: '#666', 
			  	},
			  	material: {
			    	color: 0xdedede, 
			    	fog: true, 
			  	},
			});
			texture_sprite.playable = true;
			texture_sprite.visible = false;
			if(_debug_ctype_text){
				texture_sprite.ctype ="asset_texture_sprite";
			}
			return texture_sprite;
		}



		Assets = function(){

		};
		Assets.prototype = {
			constructor:Assets, 
			test:function(){
				console.log("hello from Assets object");
			},
			start:function(){  
				this.loadFile("piso","./modelos/piso.json",true);
				this.loadFile("nave","./modelos/nave.json");
				this.loadFile("bullet","./modelos/bullet.json");
			}, 
			showElements:function(){
				if(window.w_wgl && window.w_wgl.scene){
					for(var index in w_wgl.scene.children){
						var obj = w_wgl.scene.children[index];
						if(obj.playable === true){
							obj.visible = true;
						} 
					}
				}
			}, 
			hideElements:function(){
				if(window.w_wgl && window.w_wgl.scene){
					for(var index in w_wgl.scene.children){
						var obj = w_wgl.scene.children[index];
						if(obj.playable === true){
							obj.visible = false;
						} 
					}
				}
			},
			render:function(){ 
				if(!_piso && "piso" in _assets){
					_piso = _assets["piso"];
					_piso.rotateX(Math.PI/2);
					_piso.position.z = -0.1;
				}
				if(!_nave && "nave" in _assets){
					_nave = _assets["nave"];
					_nave.up.y=0; 
					_nave.up.z=1; 
				}
			},
			getNave:function(){
				console.log("retrieving nave");
				return getCanvasNaveSprite();

				var _tmp = null;
				if(_nave){
					_tmp = _nave.clone();
					if(_debug_ctype_text){
						_tmp.ctype = "asset_nave";
						_tmp.playable = true;
					}
				}
				return _tmp;
			},
			get:function(_str){
				return _str in _assets ? _assets[_str] : null;
			},
			setLife:function(mesh){
				var canvas = document.createElement('canvas');
				var ctx = canvas.getContext('2d');
				canvas.width = canvas.height = 512;
				var texture = getCanvasTexture(canvas);
				texture.ctx = canvas.ctx = ctx;
				texture.canvas = canvas;

				/*
				var geometry = new THREE.PlaneGeometry( 1,1,2 );
				var material = new THREE.MeshBasicMaterial( {map:texture, side: THREE.FrontSide} );
				var plane = new THREE.Mesh( geometry, material );
				mesh.life_mesh = plane;
				mesh.life_mesh.texture = texture;
				*/

				var spriteMaterial = new THREE.SpriteMaterial( { map: texture, transparent:true } );

				mesh.life_mesh = new THREE.Sprite( spriteMaterial );
				mesh.life_mesh.texture = texture;
				mesh.life_mesh.ctype = "asset_sprite_life";
				mesh.life_mesh.playable = true;
				mesh.life_mesh.visible = false;
			},
			drawLife:function(mesh){ 
				if(typeof mesh.life =="undefined"){
					mesh.life = 100.0; 
				} 
				var lifeTexture = mesh.life_mesh.texture;
				var defFillStyle = lifeTexture.ctx.fillStyle;
				lifeTexture.ctx.fillStyle = "#FFFFFF";
				lifeTexture.ctx.fillRect(0,0,lifeTexture.canvas.width,60);
				lifeTexture.ctx.fillStyle = "#00FF00";
				var _w = lifeTexture.canvas.width*(mesh.life/100.0);
				lifeTexture.ctx.fillRect(0,0,_w,60); 
				lifeTexture.ctx.fillStyle = defFillStyle; 
				lifeTexture.needsUpdate = true;
				if(lifeTexture.version > 100000)lifeTexture.version = 1;
			},
			animate:function(){ 
			}, 
			loadFile:function(idx,obj,_append){
				_loader.load( 
					obj,  
					function ( geometry, materials ) { 
						var object;  
						/*for(var _index in materials){
							var _mat = materials[_index]; 
							_mat.side = THREE.FrontSide;
						} */
						/*
						for(var index in w_wgl.scene.children){
							var obj = w_wgl.scene.children[index];
							if(obj.playable && obj.visible){
								console.log(obj,obj.ctype,obj.type);
							} 
						}
						*/
						object = new THREE.Mesh( geometry, materials );  
						object.ctype = idx;
						if(idx!="piso"){
							object.playable = true;
						}
						if(_append){
							w_wgl.scene.add( object );  
						}
						_assets[idx] = object;
					},
					function(){

					},
					function(xhr){
						console.log(xhr);
					}
				);
			},
			getPlayerNameMesh:function(text){ 
			    /*
			    var texture = new THREE.Texture(createTextCanvas(text,'black','italic',20)); 
			    texture.needsUpdate = true;
			    var material = new THREE.MeshBasicMaterial({ map: texture,transparent:true});
			    var mesh = new THREE.Mesh( new THREE.PlaneGeometry(2,2,4,4), material );
			    mesh.rotateX(-2*Math.PI);
			    */
			    var mesh = getTextSprite(text,0.5);
			    mesh.text = text;
			    return mesh;
			},
		};

		return {
			instance:function(){
				if(!_instance){
					_instance = new Assets(); 
				}
				return _instance;
			}
		};
	}
)();
