var appgame = appgame || {};

appgame.PlayPause = (
	function(){

		var _instance = null; 

		var _btn_pause_src = "./imgs/boton_pause.png";
		var _btn_play_src = "./imgs/boton_play.png";

		var _btn_pause = null;
		var _btn_play = null;  

		var _width = 90;
		var _height = 90;

		var _radius = 30;

		var _width2 = _radius*2;
		var _height2 = _radius*2;

		var _point = {x:10,y:10,offsetX:10,offsetY:100}; 
		
		var _touch_identifier = null;

		window._lib_game_paused = true;

		PlayPause = function(){

		};
		PlayPause.prototype = {
			constructor:PlayPause, 
			test:function(){
				console.log("hello from PlayPause");
			},
			start:function(){
				this.cargarSprites();
				this.configurarPosicionSprites();
				this.setListener();
			},
			setListener:function(){
				var _this = this;
				if(w_ctouch){
					$(w_ctouch).click(
						function(_evt){
							_this.listen(_evt);
							_evt.preventDefault();
						}
					);
				} 
				
			}, 
			listen:function(evt){ 
				var _v2a = new THREE.Vector2(_point.x+_width2/2,_point.y+_height2/2);
				var _v2b = new THREE.Vector2(evt.clientX,evt.clientY);
				if(_v2a.distanceTo(_v2b)<=_radius){  
					w_ctouch._lib_play_pause_clicked = true;
					_lib_game_paused = !_lib_game_paused;
				}else{
					w_ctouch._lib_play_pause_clicked = false;
				}
			}, 
			render:function(){
				this.dibujarSprites();
				w_ctouch._lib_play_pause_clicked = false;
			}, 
			resize:function(){
				this.configurarPosicionSprites();
			},
			configurarPosicionSprites:function(){
				_point.x = w_width - _width2 - _point.offsetX;
				_point.y = _point.offsetY;
			},
			cargarSprites:function(){
				if(!w_ctouch_ctx)return; 
				_btn_pause = new Image;
				_btn_pause.onload=function(){
					_btn_pause._loaded = true;
				};	
				_btn_pause.src = _btn_pause_src;
				_btn_play = new Image; 
				_btn_play.onload=function(){
					_btn_play._loaded = true;
				};	
				_btn_play.src = _btn_play_src; 
			},
			dibujarSprites:function(){ 
				if(_lib_game_paused){
					if(w_ctouch_ctx && _btn_play && _btn_play._loaded){  
						w_ctouch_ctx.beginPath();
						w_ctouch_ctx.drawImage(_btn_play,0,0,_width,_height,_point.x,_point.y,_width2,_height2);
						w_ctouch_ctx.closePath();
					} 
				}else{
					if(w_ctouch_ctx && _btn_pause && _btn_pause._loaded){  
						w_ctouch_ctx.beginPath();
						w_ctouch_ctx.drawImage(_btn_pause,0,0,_width,_height,_point.x,_point.y,_width2,_height2);
						w_ctouch_ctx.closePath();
					} 	
				} 
			},
			touch:function(_what,_evt){
				return;
				if(_what=="s")this.touch_start(_evt);
				if(_what=="e")this.touch_end(_evt);
				if(_what=="c")this.touch_cancel(_evt);
				if(_what=="l")this.touch_leave(_evt);
				if(_what=="m")this.touch_move(_evt);
			},
			touch_point_collide:function(_touch){
				if(_touch.clientX && _touch.clientY){
					var _v2a = new THREE.Vector2(_point.x+_width2/2,_point.y+_height2/2);
					var _v2b = new THREE.Vector2(_touch.clientX,_touch.clientY);
					if(_v2a.distanceTo(_v2b)<=_radius){ 
						return true;
					}
				}
				return false;
			},
			touch_start:function(_evt){ 
				if(_touch_identifier===null){
					var _touches = _evt.changedTouches; 
					for(var _idx in _touches){
						var _touch = _touches[_idx];
						if(this.touch_point_collide(_touch)){ 
							_touch_identifier = _touch.identifier;
							break;
						}
					} 
				} 
			},
			touch_move:function(_evt){  
				if(_touch_identifier!==null){
					var _touches = _evt.changedTouches; 
					for(var _idx in _touches){
						var _touch = _touches[_idx];
						if(
							_touch_identifier==_touch.identifier 
						){
							if(this.touch_point_collide(_touch)){
								/*_touch_play = true;*/ 
							}else{
								/*_touch_play = false;*/
							}
							break;
						} 
					} 
				} 
			},
			touch_end:function(_evt){  
				if(_touch_identifier!==null){
					var _touches = _evt.changedTouches; 
					for(var _idx in _touches){
						var _touch = _touches[_idx]; 
						if(_touch_identifier==_touch.identifier){ 
							_touch_identifier = null;
							break;
						}
					} 
				}  
			},
			touch_cancel:function(_evt){  
				this.touch_end(_evt);
			},
			touch_leave:function(_evt){  
				this.touch_end(_evt);
			}
		};

		return {
			instance:function(){
				if(!_instance){
					_instance = new PlayPause();
					return _instance;
				}
				return null;
			}
		};
	}
)();
