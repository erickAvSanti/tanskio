var appgame = appgame || {};

appgame.CameraControl = (
	function(){

		var _instance = null;  

		var _btnChange = false;

		var _up = new THREE.Vector3(0,1,0);

		var _camPos = new THREE.Vector3();
		var _lookAt = new THREE.Vector3();
		var _lookAtDir = new THREE.Vector3();

		var _cameraYPosMin = 3;

		var _debug = false;

		var _dotScalar = 0;

		var _touch_identifier = null;
		var _touch_identifier_move_keys = null;
		var _touch_identifier_for_fire = null;

		var _touchCurrentPoint = new THREE.Vector2();
		var _touchLastPoint = new THREE.Vector2();

		var _keyA,_keyS,_keyW,_keyD,_keyN,_keySpace=false;
		  

		var _vFront_def = 90/1000;
		var _vRight_def = _vFront_def;

		var _v0Front = _vFront_def;
		var _vfFront = _vFront_def;
		var _accFront = 50/1000;

		var _v0Right = _vRight_def;
		var _vfRight = _vRight_def;
		var _accRight = 50/1000;

 
		var _dt = 20/1000;

		var _a360 = 2*Math.PI;
		var _radCircle = 33;
		var _radCircleExt = _radCircle-5;
		var _radMasterCircle = 90;
		var _points = {
			left:{x:0,y:0},			
			top:{x:0,y:0},			
			bottom:{x:0,y:0},			
			right:{x:0,y:0},
			fire:{x:0,y:0},
			x:0,
			y:0,
			touchedX:0,			
			touchedY:0			
		};
		window._lib_touchedForFire = false;

		var _tmpV2_p1 = new THREE.Vector2();
		var _tmpV2_p2 = new THREE.Vector2();

		var _supportsTouch = null;

		var _jump = {
			running:false,
			v0:0,
			v0_def:-200/1000,
			vf:0,
			vf_def:0,
			acc:0,
			acc_def:500/1000,
			last_acc:0
		};
 

		var _shooter = undefined;

		var _cameraBounding = undefined;

		var _start_running_gravity = false;

		var _debug_console = false;


		var _cameraAngle_def = 60;
		var _cameraAngle = _cameraAngle_def;

		var _zoom_value = 1;
 
 		var _limitXY = 35;

 		window._lib_slice = 1.5;

		CameraControl = function(){

		};
		CameraControl.prototype = {
			constructor:CameraControl, 
			test:function(){
				console.log("hello from CameraControl");
			},
			supportsTouch:function(){
				if(_supportsTouch===null){
					_supportsTouch = "ontouchstart" in window;
				}
				return _supportsTouch;
			},
			start:function(){  
				if ("onpointerlockchange" in document) {

					var _this = this;
					$(window).keyup(function(_evt){_this.listenKeyUp(_evt);});  
					$(window).keydown(function(_evt){_this.listenKeyDown(_evt);});   

					$(w_ctouch).click(
						function(_evt){
							if( 
								!w_ctouch._lib_fullscreen_clicked && 
								!w_ctouch._lib_play_pause_clicked
							){
								_this.touchClick(_evt);
							} 
						}
					);
					$(w_ctouch).mousewheel(function(_evt) {
					    _this.listenWhell(_evt);
					});
					(_cameraBounding = appgame.CameraBounding.instance(this)).start();

					document.addEventListener('pointerlockchange', _this.changeLock, false);
					document.addEventListener('mozpointerlockchange', _this.changeLock, false);
					document.addEventListener('webkitpointerlockchange', _this.changeLock, false);
					document.addEventListener('pointerlockerror', _this.errorLock, false);
					document.addEventListener('mozpointerlockerror', _this.errorLock, false);
					document.addEventListener('webkitpointerlockerror', _this.errorLock, false);

					document.addEventListener("mousemove",_this.mouseMove, false);
				} 
				this.setMinorHeight();
				this.calcPoints();

				(_shooter = appgame.Shooter.instance(this)).start(); 
				$( "#master_hand" ).on( "slidechange",function(event, ui){
					var _val = ui.value/100;
					_lib_slice = 1+_val*2;
				});

			}, 
			listenWhell:function(_evt){
				//console.log(event.deltaX, event.deltaY, event.deltaFactor);
				if(_evt.deltaY>0){
					_zoom_value++;
				}else{
					_zoom_value--;
				}
				if(_zoom_value<1)_zoom_value = 1;
				if(_zoom_value>25)_zoom_value = 25; 
			},
			touchClick:function(_evt){
				if(!this.hasPointerLock()){
					this.requestPointerLock();
				}
			},
			changeLock:function(_evt){
				var _this = this; 
			},
			errorLock:function(_evt){

			},
			resize:function(){
				this.setMinorHeight();
				this.calcPoints();
			},
			setMinorHeight:function(){ 
				if(w_height<=300){
					_radCircle = 33-5;
					_radCircleExt = _radCircle-5;
					_radMasterCircle = 90-5;
				}else{ 
					_radCircle = 33;
					_radCircleExt = _radCircle-5;
					_radMasterCircle = 90;
				} 
			},
			calcPoints:function(){ 
				_points.top.x = 120;
				_points.top.y = w_height-170;
				_points.bottom.x = 120;
				_points.bottom.y = w_height-70; 
				_points.left.x = 70; 
				_points.left.y = w_height-120; 
				_points.right.x = 170; 
				_points.right.y = w_height-120; 
				_points.fire.x = 45; 
				_points.fire.y = w_height-240; 
				_points.x = 120; 
				_points.y = w_height-120; 

				if(w_height<=300){ 
					_points.top.y +=30;
					_points.bottom.y +=30;
					_points.left.y +=30;
					_points.right.y +=30;
					_points.fire.y +=40;
					_points.y +=30;
				} 

			},
			mouseMove:function(_evt){
				if(_instance.hasPointerLock()){
				  	var x = _evt.movementX;
				  	var y = _evt.movementY; 
				  	_instance.rotView(x,y);
				}
			},
			listenKeyUp:function(_evt){
				var _keyCode = _evt.which || _evt.keyCode;  
				if(_keyCode==75){//keyK
					_btnChange = !_btnChange;
				}   
				if(_keyCode==78){//keyN
					_keyN = !_keyN;
				}   
				if(_keyCode==87)_keyW = false;
				if(_keyCode==83)_keyS = false;
				if(_keyCode==68)_keyD = false;
				if(_keyCode==65)_keyA = false;  
			}, 
			listenKeyDown:function(_evt){
				var _keyCode = _evt.which || _evt.keyCode;  
				if(_keyCode==87)_keyW = true;
				if(_keyCode==83)_keyS = true;
				if(_keyCode==68)_keyD = true;
				if(_keyCode==65)_keyA = true; 
				if(_keyCode==32)_keySpace = true;
			},
			obtenerVectorComponente:function(a,b){
				b = b.clone();
				b.normalize();
				var _scalar = a.x*b.x + a.y*b.y+a.z*b.z;
				return b.multiplyScalar(_scalar);
			},
			render:function(){
				if(_btnChange || window._lib_force_exit_pointer_lock){
					this.exitPointerLock();
					_btnChange = false;
					window._lib_force_exit_pointer_lock = false;
				}  
				if(_touch_identifier!==null){
					if(
						_touchCurrentPoint.x!=0 && 
						_touchCurrentPoint.y!=0 && 
						!_touchCurrentPoint.equals(_touchLastPoint)
					){
						_instance.rotView(
							(_touchCurrentPoint.x - _touchLastPoint.x)*_lib_slice,
							(_touchCurrentPoint.y - _touchLastPoint.y)*_lib_slice
						);
					}
				} 
				if(_zoom_value!=w_wgl.camera.zoom){
					w_wgl.camera.zoom = _zoom_value;
					w_wgl.camera.updateProjectionMatrix();
				}
				this.translate();
				if(_cameraBounding){
					_cameraBounding.checkCollisions(); 
				}
				if(w_wgl.camera.position.x>_limitXY)w_wgl.camera.position.x = _limitXY;
				if(w_wgl.camera.position.x<-_limitXY)w_wgl.camera.position.x = -_limitXY;
				if(w_wgl.camera.position.z>_limitXY)w_wgl.camera.position.z = _limitXY;
				if(w_wgl.camera.position.z<-_limitXY)w_wgl.camera.position.z = -_limitXY;
				if(this.supportsTouch() || _keyN){
					w_ctouch_ctx.fillStyle = 'rgba(255,255,255,0.1)';
					w_ctouch_ctx.strokeStyle = 'rgba(0,0,0,0.3)';
					w_ctouch_ctx.lineWidth = 2;
					w_ctouch_ctx.beginPath();
					w_ctouch_ctx.arc(_points.x,_points.y,_radMasterCircle,0,_a360);
					w_ctouch_ctx.fill();
					w_ctouch_ctx.stroke();
					w_ctouch_ctx.closePath();
					//-----------------------------
					//left
					w_ctouch_ctx.beginPath();
					w_ctouch_ctx.arc(_points.left.x,_points.left.y,_radCircle,0,_a360);
					w_ctouch_ctx.fill();
					w_ctouch_ctx.stroke();
					w_ctouch_ctx.closePath();
					//-----------------------------
					//top
					w_ctouch_ctx.beginPath();
					w_ctouch_ctx.arc(_points.top.x,_points.top.y,_radCircle,0,_a360);
					w_ctouch_ctx.fill();
					w_ctouch_ctx.stroke();
					w_ctouch_ctx.closePath();
					//-----------------------------
					//bottom
					w_ctouch_ctx.beginPath();
					w_ctouch_ctx.arc(_points.bottom.x,_points.bottom.y,_radCircle,0,_a360);
					w_ctouch_ctx.fill();
					w_ctouch_ctx.stroke();
					w_ctouch_ctx.closePath();
					//-----------------------------
					//right
					w_ctouch_ctx.beginPath();
					w_ctouch_ctx.arc(_points.right.x,_points.right.y,_radCircle,0,_a360);
					w_ctouch_ctx.fill();
					w_ctouch_ctx.stroke();
					w_ctouch_ctx.closePath();
					//----------------------------- 
					//fire
					if(_lib_touchedForFire){
						w_ctouch_ctx.fillStyle = 'rgba(0,0,255,0.4)';
					}else{ 
						w_ctouch_ctx.fillStyle = 'rgba(255,255,255,0.4)';
					}
					w_ctouch_ctx.beginPath();
					w_ctouch_ctx.arc(_points.fire.x,_points.fire.y,_radCircle,0,_a360);
					w_ctouch_ctx.fill();
					w_ctouch_ctx.stroke();
					w_ctouch_ctx.closePath();
					//----------------------------- 
					if(
						_points.touchedX!=0 && 
						_points.touchedY!=0
					){
						w_ctouch_ctx.beginPath();
						w_ctouch_ctx.arc(_points.touchedX,_points.touchedY,_radCircleExt,0,_a360);
						w_ctouch_ctx.fill();
						w_ctouch_ctx.stroke();
						w_ctouch_ctx.closePath();
					}
					
				}
				//-----------------------------
				w_ctouch_ctx.fillStyle = 'rgba(255,255,255,0.4)';  
				w_ctouch_ctx.beginPath();
				w_ctouch_ctx.rect(w_width/2-50,w_height/2-0.5,100,1);
				w_ctouch_ctx.fill(); 
				w_ctouch_ctx.closePath();
				//----------------------------- 
				w_ctouch_ctx.beginPath();
				w_ctouch_ctx.rect(w_width/2-0.5,w_height/2-50,1,100); 
				w_ctouch_ctx.fill(); 
				w_ctouch_ctx.closePath();
				//-----------------------------
				if(_shooter)_shooter.render();
			},
			translate:function(){

				if(!w_wgl.camera._lib_last_position){
					w_wgl.camera._lib_last_position = new THREE.Vector3();
				}
				w_wgl.camera._lib_last_position.copy(w_wgl.camera.position);

				var _movFront = 0;
				var _movRight = 0;

				if(_keyW)_movFront+=1;
				if(_keyS)_movFront+=-1;
				if(_keyD)_movRight+=1;
				if(_keyA)_movRight+=-1;
 
				if( _keySpace && _jump.acc==0 ){ 
					_jump.v0 = _jump.v0_def;   
					_jump.acc = _jump.acc_def;  
				}  
				this.applyGravity();  
				_keySpace = false; 
				if(_movFront!=0 || _movRight!=0){

					var _wdir = w_wgl.camera.getWorldDirection().clone().normalize();
					if(_movRight!=0){  
						if(_vfRight<1)_vfRight = _v0Right + _accRight*_dt;    
						var _wrightDir = _wdir.clone().cross(_up).normalize();
						_wrightDir.multiplyScalar(_movRight*_vfRight);
						_v0Right = _vfRight;
						w_wgl.camera.position.add(_wrightDir); 
					}else{
						_vfRight = _v0Right = _vRight_def;
					}
					if(_movFront!=0){  
						if(_vfFront<1)_vfFront = _v0Front + _accFront*_dt;  
						//-------------------------------------------------- 
						var _tmpWdir = this.obtenerVectorComponente(_wdir,_up);
						_wdir.sub(_tmpWdir).normalize(); 
						//--------------------------------------------------
						_wdir.multiplyScalar(_movFront*_vfFront);
						_v0Front = _vfFront; 
						w_wgl.camera.position.add(_wdir); 
					}else{
						_vfFront = _v0Front = _vFront_def;
					}
				}else{
					_vfFront = _v0Front = _vFront_def;
					_vfRight = _v0Right = _vRight_def;
				}
				window.___vfFront = _vfFront;
				window.___v0Front = _v0Front;
				window.___vfRight = _vfRight;
				window.___v0Right = _v0Right;
				window.___v0 = _jump.v0;
				window.___vf = _jump.vf;
				window.___acc = _jump.acc;
				window.___last_acc = _jump.last_acc; 
			}, 
			applyGravity:function(){ 
				this.checkFloorPosition();
				if(_jump.acc!=0){ 
					_jump.vf = _jump.v0 + _jump.acc * _dt; 
					_jump.v0 = _jump.vf;  
					w_wgl.camera.position.sub(_up.clone().multiplyScalar(_jump.vf));
					this.checkFloorPosition();
				}   
			},
			checkFloorPosition:function(){ 
				if(w_wgl.camera.position.y<_cameraYPosMin){ 
					w_wgl.camera.position.y = _cameraYPosMin;  
					_jump.v0 = 0;
					_jump.acc = 0;  
				}  
			},
			startRunningGravity:function(){
				if(!_start_running_gravity){ 
					this.startGravity(_start_running_gravity = true);
				} 
			},
			disableRunningGravity:function(prevent){
				_start_running_gravity = false; 
				if(prevent){
					if(_jump.v0>=0)this.setV0VfZero();
				}else{
					this.setV0VfZero();
				} 
			},
			resetAcc:function(){  
				if(w_wgl.camera.position.y<=_cameraYPosMin){
					this.setAccZero();
				}else{
					_jump.acc = _jump.acc_def;
				} 
			},
			setAccZero:function(){  
				_jump.acc = 0;
			},
			setV0Zero:function(){ 
				_jump.v0 = 0;
			},
			setVfZero:function(){ 
				_jump.vf = 0;
			},
			resetV0:function(){ 
				_jump.v0 = _jump.v0_def;
			},
			resetVf:function(){ 
				_jump.vf = _jump.vf_def;
			},
			setV0VfZero:function(){ 
				this.setV0Zero();
				this.setVfZero();
			},
			resetVFrontVRightZero:function(){ 
				_vfFront = _v0Front = _vFront_def;
				_vfRight = _v0Right = _vRight_def;
			},
			startGravity:function(force){
				if(_jump.v0 == 0 || (force && _jump.v0>=0))_jump.v0 = 0.000001; 
			},
			requestPointerLock:function(){
				if(!w_ctouch.requestPointerLock){
					w_ctouch.requestPointerLock = 
						w_ctouch.requestPointerLock || 
						w_ctouch.mozRequestPointerLock || 
						w_ctouch.webkitRequestPointerLock;
				} 
				if(w_ctouch.requestPointerLock){
					w_ctouch.requestPointerLock();
					console.log("solicitando bloqueo de document.");	
				} 
			},
			exitPointerLock:function(){
				if(!document.exitPointerLock){
					document.exitPointerLock = 
						document.exitPointerLock || 
						document.mozExitPointerLock || 
						document.webkitExitPointerLock;
				} 
				if(document.exitPointerLock){
					document.exitPointerLock();
					console.log("retirando bloqueo de document.");
				} 
			},
			hasPointerLock:function(){
				if(!document.pointerLockElement){
					document.pointerLockElement = 
						document.pointerLockElement || 
						document.mozPointerLockElement || 
						document.webkitPointerLockElement;
					} 
				return w_ctouch && document.pointerLockElement == w_ctouch;
			},
			collidePoints2D:function(x1,y1,x2,y2,_radius){
				_tmpV2_p1.set(x1,y1);
				_tmpV2_p2.set(x2,y2); 
				if(_tmpV2_p1.distanceTo(_tmpV2_p2)<=_radius){ 
					return true;
				} 
			},
			rotView:function(x,y){
				if(x!=0 || y!=0){
					var _frontDir = w_wgl.camera.getWorldDirection().clone().normalize();
					var _rightDir = _frontDir.clone().cross(_up).normalize(); 
					_camPos.copy(w_wgl.camera.position);
					_lookAt.set(0,0,0);
					if(_debug){
						console.log("_frontDir",_frontDir);
						console.log("_rightDir",_rightDir);
						console.log("_camPos",_camPos); 
					}
			        if(x!=0)this.rotViewAround(-x/360.0,_up,_frontDir);
			        if(y!=0)this.rotViewAround(-y/360.0,_rightDir,_frontDir); 
					if(_debug){
						console.log("_lookAt",_lookAt); 
					}
					_lookAtDir.copy(_lookAt).normalize();
			        _lookAt.add(_camPos);
					if(_debug){
						console.log("_lookAt2",_lookAt); 
					}
					_dotScalar = _up.dot(_lookAtDir);
					if(_dotScalar>=-0.995 && _dotScalar<=0.995){
						w_wgl.camera.lookAt(_lookAt);
					} 
				} 
			}, 
			rotViewAround:function(deltaAngle, axis,vecRot) {
				var _axis = vec3.create([axis.x,axis.y,axis.z]); 
				var _vecRot = vec3.create([vecRot.x,vecRot.y,vecRot.z]); 
		        var q = quat4.create();
		        quat4.fromAngleAxis(deltaAngle,_axis, q);
		        quat4.multiplyVec3(q,_vecRot);
		        vec3.normalize(_vecRot);
				if(_debug){
					console.log("_vecRot",_vecRot); 
				}
		       	_lookAt.x += _vecRot[0];
		       	_lookAt.y += _vecRot[1];
		       	_lookAt.z += _vecRot[2];  
		    },
			touch:function(_what,_evt){

				if( 
					!w_ctouch._lib_fullscreen_clicked && 
					!w_ctouch._lib_play_pause_clicked && 
					this.supportsTouch() 
				){
					if(_what=="s")this.touch_start(_evt);
					if(_what=="e")this.touch_end(_evt);
					if(_what=="c")this.touch_cancel(_evt);
					if(_what=="l")this.touch_leave(_evt);
					if(_what=="m")this.touch_move(_evt);
				}  
			},
			touch_start:function(_evt){ 
				if(
					_touch_identifier===null || 
					_touch_identifier_move_keys===null || 
					_touch_identifier_for_fire===null
				){
					var _touches = _evt.changedTouches; 
					for(var _idx in _touches){
						var _touch = _touches[_idx];
						if(
							this.collidePoints2D(
								_touch.clientX,
								_touch.clientY,
								_points.x,
								_points.y,
								_radMasterCircle
							)
						){
							if(_touch_identifier_move_keys===null){ 
								_touch_identifier_move_keys = _touch.identifier;
								this.handleTouchMoveKey(_touch.clientX,_touch.clientY);
								break;
							}
						}
						else if(
							this.collidePoints2D(
								_touch.clientX,
								_touch.clientY,
								_points.fire.x,
								_points.fire.y,
								_radMasterCircle
							)
						){
							if(_touch_identifier_for_fire===null){ 
								_touch_identifier_for_fire = _touch.identifier;
								_lib_touchedForFire = true;
								break;
							}
						}else{
							if(_touch_identifier===null){
								_touch_identifier = _touch.identifier;
								_touchCurrentPoint.set(_touch.clientX,_touch.clientY);
								_touchLastPoint.copy(_touchCurrentPoint);
								break;
							}
						}  
						
					} 
				} 
			},
			touch_move:function(_evt){  
				if(
					_touch_identifier!==null || 
					_touch_identifier_move_keys!==null || 
					_touch_identifier_for_fire!==null
				){
					var _touches = _evt.changedTouches; 
					for(var _idx in _touches){
						var _touch = _touches[_idx];
						if(_touch_identifier!==null && _touch_identifier==_touch.identifier){ 
							_touchLastPoint.copy(_touchCurrentPoint);
							_touchCurrentPoint.set(_touch.clientX,_touch.clientY);
							break;
						}
						if(_touch_identifier_move_keys!==null && _touch_identifier_move_keys==_touch.identifier){ 
							this.handleTouchMoveKey(_touch.clientX,_touch.clientY);
							break;
						} 
						if(_touch_identifier_for_fire!==null && _touch_identifier_for_fire==_touch.identifier){ 
							//this.handleTouchMoveKey(_touch.clientX,_touch.clientY);
							break;
						} 
					} 
				} 
			},
			touch_end:function(_evt){  
				if(
					_touch_identifier!==null || 
					_touch_identifier_move_keys!==null || 
					_touch_identifier_for_fire!==null
				){
					var _touches = _evt.changedTouches; 
					for(var _idx in _touches){
						var _touch = _touches[_idx]; 
						if(_touch_identifier!==null && _touch_identifier==_touch.identifier){ 
							_touch_identifier = null;
							_touchLastPoint.copy(_touchCurrentPoint);
							break;
						}
						if(_touch_identifier_move_keys!==null && _touch_identifier_move_keys==_touch.identifier){ 
							_touch_identifier_move_keys = null; 
							_keyW = false;
							_keyA = false;
							_keyS = false;
							_keyD = false;
							break;
						}
						if(_touch_identifier_for_fire!==null && _touch_identifier_for_fire==_touch.identifier){ 
							_touch_identifier_for_fire = null; 
							_lib_touchedForFire = false;
							break;
						}
					} 
				}
				_points.touchedX=_points.touchedY = 0;  
			},
			touch_cancel:function(_evt){  
				this.touch_end(_evt);
			},
			touch_leave:function(_evt){  
				this.touch_end(_evt);
			},
			handleTouchMoveKey:function(x,y){ 
				if(
					this.collidePoints2D(
						x,
						y,
						_points.x,
						_points.y,
						_radMasterCircle
					)
				){
					_points.touchedX = x;
					_points.touchedY = y;
					if(
						this.collidePoints2D(
							x,
							y,
							_points.top.x,
							_points.top.y,
							_radCircleExt*2
						)
					){
						_keyW = true;
					}else{
						_keyW = false;
					}
					if(
						this.collidePoints2D(
							x,
							y,
							_points.bottom.x,
							_points.bottom.y,
							_radCircleExt*2
						)
					){
						_keyS = true;
					}else{
						_keyS = false;
					}
					if(
						this.collidePoints2D(
							x,
							y,
							_points.left.x,
							_points.left.y,
							_radCircleExt*2
						)
					){
						_keyA = true;
					}else{
						_keyA = false;
					}
					if(
						this.collidePoints2D(
							x,
							y,
							_points.right.x,
							_points.right.y,
							_radCircleExt*2
						)
					){
						_keyD = true;
					}else{
						_keyD = false;
					}

				}else{
					_keyW = false;
					_keyS = false;
					_keyD = false;
					_keyA = false;
					_points.touchedX = 0;
					_points.touchedY = 0;
				}
			}
		};

		return {
			instance:function(){
				if(!_instance){
					_instance = new CameraControl();
					return _instance;
				}
				return null;
			}
		};
	}
)();