var appgame = appgame || {};

appgame.Bloques = (
	function(){

		var _instance = null;  

		var _loader = new THREE.JSONLoader();  
		
		Bloques = function(){
		};
		Bloques.prototype = {
			constructor:Bloques, 
			test:function(){
				console.log("hello from Bloques");
			},
			start:function(){  
				var _this = this; 
				this.setListener();
			}, 
			setListener:function(){
			}, 
			listen:function(evt){ 
			}, 
			render:function(){ 
			}, 
			resize:function(){ 
			} 
		};

		return {
			instance:function(){
				if(!_instance){
					_instance = new Bloques();
					return _instance;
				}
				return null;
			}
		};
	}
)();