var appgame = appgame || {};

appgame.LeaderBoard = (
	function(){

		var _instance = null; 
		var _assets = null;

		var _data = null;

		LeaderBoard = function(){ 
			_assets = appgame.Assets.instance();
		};
		LeaderBoard.prototype = {
			constructor:LeaderBoard, 
			test:function(){
				console.log("hello from LeaderBoard object");
			},
			start:function(){

			},
			setData:function(data){
				_data = data;
			},
			render:function(){
				if(!window.w_ctouch_ctx || !_data)return; 
				var _pos_x = 7*w_width/8;
				var _pos_y = 40; 
				if(_data.length>0){
					var _order = [];
					for(var idx in _data){
						var _el = _data[idx];
						if(_el.flagStarted)_order.push([_el.name,_el.life]);
					}	
					_order.sort(function(a,b){
						return b[1] - a[1];
					});
					var _tmp_font = w_ctouch_ctx.font;
					var _max = 5;
					var _count = 1; 
					for(var idx in _order){
						if(_count>_max)break;  
						var _obj = _order[idx];
						w_ctouch_ctx.beginPath();
						w_ctouch_ctx.font="20px Georgia";
						var _txt = _count + ". "+_obj[0];
						w_ctouch_ctx.fillText(_txt,_pos_x,_pos_y);
						w_ctouch_ctx.closePath();
						_pos_y+=30;
						_count++;
					}
					w_ctouch_ctx.font=_tmp_font;
				} 
			}
		};


		return {
			instance:function(){
				if(!_instance){
					_instance = new LeaderBoard(); 
				}
				return _instance;
			}
		};
	}
)();