
<?php 
	$protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http");
	$current_url_host = $protocol . "://$_SERVER[HTTP_HOST]";
	$current_url = $current_url_host.$_SERVER[REQUEST_URI];
	require_once 'Mobile_Detect.php';
	$detect = new Mobile_Detect;
	$host = ".";
	$mt_rand = mt_rand();

	$minify = array_key_exists("minify",$_GET) && $_GET["minify"]=="32f54g6gfdw53g4ge46g454ef5t"; 
?>

<html>
<head  prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
	<!-- Allow fullscreen mode on iOS devices. (These are Apple specific meta tags.) -->
	<meta property="fb:app_id" content="302184056577324" /> 
  	<meta property="og:type"   content="article" /> 
  	<meta property="og:url"    content="<?php echo $current_url;?>" /> 
  	<meta property="og:title"  content="Tanks IO | Arquigames" />  
  	<meta property="og:image"  content="<?php echo $current_url_host;?>/imgs/img.png" /> 
  	<meta property="og:image:width"  content="256" /> 
  	<meta property="og:image:height"  content="256" /> 


	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, minimal-ui" />
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" /> 
	<meta name="HandheldFriendly" content="true" />
	<title>Tanks IO | Arquigames</title>
	<link href="https://fonts.googleapis.com/css?family=Kurale" rel="stylesheet">

	<!--
	<link rel="apple-touch-icon" sizes="57x57" href="./imgs/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="./imgs/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="./imgs/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="./imgs/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="./imgs/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="./imgs/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="./imgs/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="./imgs/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="./imgs/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="./imgs/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="./imgs/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="./imgs/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="./imgs/favicon-16x16.png">
	<link rel="manifest" href="./manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="./imgs/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<link rel="icon" href="./imgs/favicon.ico">
	<link rel="icon" type="image/x-icon" href="./imgs/favicon.ico" />

	<link rel="icon" href="./imgs/favicon.png">
	<link rel="icon" type="image/png" href="./imgs/favicon.png" />
	-->

	<link rel="stylesheet" href="<?php echo $host;?>/font-awesome-4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $host;?>/third-party/css/bootstrap.4.1.3.css">
	<link rel="stylesheet" type="text/css" href="<?php echo $host;?>/css/main.css">

	<link href="https://fonts.googleapis.com/css?family=Fredoka+One|Bree+Serif" rel="stylesheet">

	<link rel="stylesheet" href="<?php echo $host;?>/css/jquery-ui.css"> 
	<script type="text/javascript" src="<?php echo $host;?>/third-party/js/jquery-3.2.1.js"></script> 
	<script src="<?php echo $host;?>/third-party/js/jquery-ui.js"></script> 
	<script type="text/javascript" src="<?php echo $host;?>/third-party/js/randomColor.js"></script>
	<script type="text/javascript" src="<?php echo $host;?>/third-party/js/isMobile.js"></script>
	<script type="text/javascript" src="<?php echo $host;?>/third-party/js/underscore.js"></script> 
	<script type="text/javascript" src="<?php echo $host;?>/third-party/js/bootstrap.4.1.3.js"></script>
	<script type="text/javascript" src="<?php echo $host;?>/third-party/js/jquery.mousewheel.js"></script> 
	<script type="text/javascript" src="<?php echo $host;?>/third-party/js/Stats.js"></script> 
	<script type="text/javascript" src="<?php echo $host;?>/third-party/js/glMatrix.js"></script> 
	<script type="text/javascript" src="<?php echo $host;?>/third-party/js/three.js"></script>   
	<script type="text/javascript" src="<?php echo $host;?>/third-party/js/THREE.TextTexture.js"></script> 
	<script type="text/javascript" src="<?php echo $host;?>/third-party/js/THREE.TextSprite.js"></script>  

	<?php if(!$minify && $protocol=="http"):?>
	<script type="text/javascript" src="<?php echo $host;?>/js/leaderBoard.js"></script>
	<script type="text/javascript" src="<?php echo $host;?>/js/fullscreen.js"></script>
	<script type="text/javascript" src="<?php echo $host;?>/js/realtime.js"></script>  
	<script type="text/javascript" src="<?php echo $host;?>/js/assets.js"></script>  
	<script type="text/javascript" src="<?php echo $host;?>/js/enemies.js"></script>  
	<script type="text/javascript" src="<?php echo $host;?>/js/main.js"></script>
	<?php else:?>
		<script type="text/javascript" src="<?php echo $host;?>/dist/merged.min.js?v=<?php echo $mt_rand;?>"></script>
	<?php endif;?>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-77863190-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-77863190-1');
	</script>
 

</head>
<body> 
	<div id="container">
		<canvas id="webgl"></canvas>
		<canvas id="canvas-assets"></canvas>
		<canvas id="canvas-touch"></canvas> 
	</div> 
	<?php 
		include "modals.php";
	?>
	<div id="fb-root"></div>
	<script>
		(function(d, s, id) {
	  		var js, fjs = d.getElementsByTagName(s)[0];
	  		if (d.getElementById(id)) return;
	  		js = d.createElement(s); js.id = id;
	  		js.src = 'https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v2.11&appId=728135517275940';
	  		fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	<script async defer src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script> 
	<script>
		if(!/tanks-io\.games/.test(window.location.host)){
		    (adsbygoogle = window.adsbygoogle || []).onload = function () {
		        [].forEach.call(document.getElementsByClassName('adsbygoogle'), function () {
		            adsbygoogle.push({})
		        })
		    }
		}
	</script> 
	<div id='settings'></div>
</body>
</html>