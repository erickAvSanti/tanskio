// Require gulp
var gulp = require('gulp');

// Require plugins
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var gutil = require('gulp-util');
var grename = require('gulp-rename');
var strip = require('gulp-strip-comments');
var greplace = require('gulp-replace');


gulp.task('scripts', function() {
    return gulp.src([
    		'./js/*.js',
            '!./js/bloques.js',
            '!./js/playPause.js',
            '!./js/cameraBounding.js',
    		'!./js/cameraControl.js',
    	])
        .pipe(concat('merged.js'))
        .pipe(gulp.dest('./dist'))
        .pipe(grename('merged.min.js'))
        .pipe(strip())
        .pipe(greplace("LeaderBoard","aLBc"))
        .pipe(greplace("FullScreen","bFSn"))
        .pipe(greplace("Assets","Ats"))
        .pipe(greplace("CameraBounding","BCb"))
        .pipe(greplace("CameraControl","hCCl"))
        .pipe(greplace("Enemies","EnS"))
        .pipe(greplace("PlayPause","PyPe"))
        .pipe(greplace("Realtime","RtE"))
        .pipe(greplace("instance","_ist_")) 
        .pipe(greplace("touch_start","t_st")) 
        .pipe(greplace("touch_end","t_en")) 
        .pipe(greplace("touch_leave","t_lve")) 
        .pipe(greplace("touch_cancel","t_cnl")) 
        .pipe(greplace("getCanvasTexture","gCTx")) 
        .pipe(greplace("createTextCanvas","cTxC")) 
        .pipe(greplace("getTextSprite","gTxSp")) 
        .pipe(greplace("getNave","gNv")) 
        .pipe(greplace("setLife","sLf")) 
        .pipe(greplace("drawLife","dLf")) 
        .pipe(greplace("getPlayerNameMesh","gPnM")) 
        .pipe(greplace("composeVector3","cV3")) 
        .pipe(greplace("obtenerVectorComponente","obV3C")) 
        .pipe(greplace("intersectionSphereSphere","insSph")) 
        .pipe(greplace("supportsTouch","sppT")) 
        .pipe(greplace("touchClick","t_clk")) 
        .pipe(greplace("listenKeyDown","lstKDwn")) 
        .pipe(greplace("applyGravity","apyGrav")) 
        .pipe(greplace("checkFloorPosition","ckFPos")) 
        .pipe(greplace("rotView","rVw")) 
        .pipe(greplace("rotViewAround","rVwAd")) 
        .pipe(greplace("parseBullets","pBlls")) 
        .pipe(greplace("parseClients","pCls")) 
        .pipe(greplace("configurarPosicionSprites","cfgPsSptr")) 
        .pipe(greplace("dibujarSprites","dwSptr")) 
        .pipe(greplace("showPresentation","swPrest")) 
        .pipe(uglify())
        .on('error', function (err) { gutil.log(gutil.colors.red('[Error]'), err.toString()); })
        .pipe(gulp.dest('./dist'));
});