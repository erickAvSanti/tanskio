// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'  
 
import BootstrapVue from 'bootstrap-vue'; 

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './assets/css/common.css'

Vue.config.productionTip = false

global.jQuery = require('jquery'); 
global.THREE = require('three'); 
global.mouse2Dto3D = require('./assets/js/mouse2Dto3D'); 
global.degrees = require('radians-degrees');
global.radians = require('degrees-radians');
Vue.use(BootstrapVue);
Vue.use(THREE); 

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { 
  	App, 
  },
  template: '<App/>'
})
