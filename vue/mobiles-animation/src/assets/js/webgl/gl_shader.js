function loadShader(gl, type, source,cb) {
  const shader = gl.createShader(type);

  // Send the source to the shader object

  gl.shaderSource(shader, source);

  // Compile the shader program

  gl.compileShader(shader);

  // See if it compiled successfully

  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    if(cb)cb('An error occurred compiling the shaders: ' + gl.getShaderInfoLog(shader)+', source: '+source);
    gl.deleteShader(shader);
    return null;
  }

  return shader;
}
function initShaderProgram(gl, vsSource, fsSource,cb) {
  const vertexShader = loadShader(gl, gl.VERTEX_SHADER, vsSource,cb);
  const fragmentShader = loadShader(gl, gl.FRAGMENT_SHADER, fsSource,cb);
  if(!vertexShader || !fragmentShader)return null;
  // Create the shader program

  const shaderProgram = gl.createProgram();
  gl.attachShader(shaderProgram, vertexShader);
  gl.attachShader(shaderProgram, fragmentShader);
  gl.linkProgram(shaderProgram);

  // If creating the shader program failed, alert

  if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
    if(cb)cb('Unable to initialize the shader program: ' + gl.getProgramInfoLog(shaderProgram));
    return null;
  } 
  return shaderProgram;
}
module.exports = {
  loadShader:loadShader,
  initShaderProgram:initShaderProgram,
};