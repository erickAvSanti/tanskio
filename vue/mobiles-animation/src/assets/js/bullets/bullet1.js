Bullet = function(player){
	this.player = player;
	this.visible = false;
	this.position = new THREE.Vector2();
	this.vUnit = new THREE.Vector2();
	this.angle = 0;
	this.velocity = 10; 
	this.radius = 10; 
	this.crashed = false; 
	this.angle_arc = 2*Math.PI;
}
Bullet.prototype = {
	draw:function(ctx){

		this.position.addScaledVector(this.vUnit,this.velocity);

		ctx.beginPath();
		ctx.fillStyle="#FFFFFF";
		ctx.arc(
			this.position.x,
			this.position.y,
			this.radius,
			0,
			this.angle_arc
		);
		ctx.fill();
	}
};
module.exports = Bullet;