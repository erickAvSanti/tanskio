Explosion = function(){
	this.uuid = uuidv4();
	this.animation = {
		from:0.0,
		from_def:0.0,
		to:100.0,
		to_def:100.0,
		step:0.0,
		inc:5.0,
		flip:70.0, 
		incRot:Math.PI/180,
		center1:new THREE.Vector2(),
		center2:new THREE.Vector2(),
		radius1:2,
		radius2:30,
		radius:30,
	};  
	this.Rot360g = 2*Math.PI;
	this.position = new THREE.Vector2(); 
	this.visible = true;
}
Explosion.prototype ={  
	reset:function(){
		this.animation.from = this.animation.from_def;
		this.animation.to = this.animation.to_def;
		this.visible = true;
	},
  	animFunc:function (){
		if(this.animation.step<this.animation.to){
			this.animation.step+=this.animation.inc;
		}else{
			this.animation.step = this.animation.from;
			this.visible = false;
		}
	},
	getFlippedAnimOpacity:function (){
		var _opacity2 = 0;
		if(this.animation.step<this.animation.flip){
			_opacity2 = this.animation.step/this.animation.flip; 
		}else{
			var _diff1 = this.animation.to-this.animation.flip;
			var _step2 = this.animation.to-this.animation.step;
			_opacity2 = _step2/_diff1;
		}
		return _opacity2;
	},
	draw:function(ctx){  
		this.animation.center1.copy(this.position);
		this.animation.center2.copy(this.position);
		this.animFunc();
		var _opacity2 =  this.getFlippedAnimOpacity();
		var _opacity = this.animation.step/100.0; 
		var grd3=ctx.createRadialGradient(
			this.animation.center1.x,
			this.animation.center1.y,
			this.animation.radius1,
			this.animation.center2.x,
			this.animation.center2.y,
			this.animation.radius2
		);
		grd3.addColorStop(0,`rgb(0,0,0,0)`);
		grd3.addColorStop(1,`rgb(255,255,255,${_opacity2})`);
		//------------------------------------------
		ctx.beginPath();
		ctx.fillStyle=grd3;  
		ctx.arc(this.position.x,this.position.y,Math.round(this.animation.radius*_opacity),0,this.Rot360g);  
		ctx.fill();       
	}
};
module.exports = Explosion;