Shooter = function(player){
	this.uuid = uuidv4(); 
	this.player = player; 
	this.bullets = []; 
	this.explosions = []; 
	this.paddingX = 30;
	this.paddingY = 30;
};
Shooter.prototype={
	shoot:function(){
		if(this.player.vectorDirection.length()<0.01)return;
		var bullet = this.getBullet();
		bullet.position.copy(this.player.position);
		bullet.vUnit.copy(this.player.vectorDirection).normalize();
		bullet.angle = this.player.angle;
		bullet.visible = true;
		console.log(bullet);
	},
	getBullet:function(){ 
		var find = false,bullet;
		for(var idx in this.bullets){
			bullet = this.bullets[idx];
			if(!bullet.visible){
				find = true;
				break;
			}
		}
		if(!find){
			bullet = new Bullet(this.player);
			this.bullets.push(bullet);
		}
		return bullet;
	},
	newExplosion1:function(bullet){  

		var find = false,exp;
		for(var idx in this.explosions){
			exp = this.explosions[idx];
			if(!exp.visible){
				find = true;
				break;
			}
		}
		if(!find){
			exp = new Explosion1();
			this.explosions.push(exp);
		}else{
			exp.reset();
		} 
		exp.position.copy(bullet.position);
		return exp;
	},
	draw:function(ctx){
		var _this = this;
		this.bullets.map(function(bullet){
	 		if(bullet.visible)_this.checkOutside(bullet,ctx);
			if(bullet.visible){
				bullet.draw(ctx);
			}else{
				if(bullet.crashed){
					_this.newExplosion1(bullet);
					bullet.crashed = false;
				}
			}
		});
		for(var idx in this.explosions){
			var exp = this.explosions[idx];
			if(exp.visible){
				exp.draw(ctx);
			} 
		}  
	},
	checkOutside(bullet,ctx){ 
		if(bullet.position.x<this.paddingX || bullet.position.x>ctx.width-this.paddingX){
			bullet.visible = false;
		}
		if(bullet.position.y<this.paddingY || bullet.position.y>ctx.height-this.paddingY){
			bullet.visible = false;
		}
		if(!bullet.visible){
			bullet.crashed = true;
		}
	},
};