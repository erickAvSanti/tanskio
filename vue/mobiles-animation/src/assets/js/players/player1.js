Player = function(){
	this.uuid = uuidv4();
	this.animation = {
		from:0.0,
		to:100.0,
		step:0.0,
		inc:2.0,
		flip:70.0, 
		incRot:Math.PI/180,
		center1:new THREE.Vector2(),
		center2:new THREE.Vector2(),
		radius1:2,
		radius2:30,
		radius:30,
	}; 
	this.angle = 0;
	this.Rot360g = 2*Math.PI;
	this.position = new THREE.Vector2();
	this.mousePoint = new THREE.Vector2();
	this.mousePoint_tmp = new THREE.Vector2(-9999999,-9999999);
	this.vectorDirection = new THREE.Vector2();
	this.shooter = new Shooter(this);
}
Player.prototype ={ 
	detectEvents:function(keys){
		if(keys.s){
			console.log("...shoot");
			this.shooter.shoot();
			keys.s = false;
		}
	},
  	animFunc:function (){
		if(this.animation.step<this.animation.to){
			this.animation.step+=this.animation.inc;
		}else{
			this.animation.step = this.animation.from;
		}
	},
	getFlippedAnimOpacity:function (){
		var _opacity2 = 0;
		if(this.animation.step<this.animation.flip){
			_opacity2 = this.animation.step/this.animation.flip; 
		}else{
			var _diff1 = this.animation.to-this.animation.flip;
			var _step2 = this.animation.to-this.animation.step;
			_opacity2 = _step2/_diff1;
		}
		return _opacity2;
	},
	draw:function(ctx){ 
		this.animation.center1.copy(this.position);
		this.animation.center2.copy(this.position);
		this.animFunc();
		var _opacity2 =  this.getFlippedAnimOpacity();
		var _opacity = this.animation.step/100.0; 
		var grd3=ctx.createRadialGradient(
			this.animation.center1.x,
			this.animation.center1.y,
			this.animation.radius1,
			this.animation.center2.x,
			this.animation.center2.y,
			this.animation.radius2
		);
		grd3.addColorStop(0,`rgb(0,0,0,0)`);
		grd3.addColorStop(1,`rgb(255,255,255,${_opacity2})`);
		//------------------------------------------
		ctx.beginPath();
		ctx.fillStyle=grd3;  
		ctx.arc(this.position.x,this.position.y,Math.round(this.animation.radius*_opacity),0,this.Rot360g);  
		ctx.fill();      
		//------------------------------------------
		if(!this.mousePoint_tmp.equals(this.mousePoint)){
			this.vectorDirection.copy(this.mousePoint).sub(this.position);
			this.mousePoint_tmp.copy(this.mousePoint);

			this.angle = Math.atan2(this.vectorDirection.y,this.vectorDirection.x)*180.0/Math.PI; 
			if(this.angle<0){
				this.angle = 360 + this.angle;
			} 
		}

		ctx.save();
		ctx.beginPath();
		ctx.translate(this.position.x,this.position.y);
		ctx.rotate(this.angle * Math.PI / 180.0);
		ctx.fillStyle="#FF0000";
		ctx.rect(
			-10,
			-5,
			20,
			10
		);
		ctx.fill();
		ctx.restore();
		this.shooter.draw(ctx);
	}
};
module.exports = Player;