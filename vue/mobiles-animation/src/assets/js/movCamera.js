var vUnit = new THREE.Vector2();
var vUnit_tmp = new THREE.Vector2();
var cam2Dpos = new THREE.Vector2(); 
var mouse2D = new THREE.Vector2();
var mouse3D_tmp = new THREE.Vector3(-9999999,-9999999);

var lastCameraPosition = new THREE.Vector2();

var paddingX = 20;
var paddingY = 20;

var flag = false;

module.exports = function(camera,ctx,mouse3D,mouse2Dcanvas,cam2Dcanvas){
	mouse2D.x = mouse3D.x;
	mouse2D.y = mouse3D.y;
	cam2Dpos.x = camera.position.x;
	cam2Dpos.y = camera.position.y;
	lastCameraPosition.copy(cam2Dpos);

	var distance = mouse2D.distanceTo(cam2Dpos);
	if(distance>10 && !mouse3D.equals(mouse3D_tmp)){ 
		vUnit.copy(mouse2D).sub(cam2Dpos).normalize();
		vUnit_tmp.copy(vUnit);
	}else{
		vUnit.copy(vUnit_tmp);
	} 
	cam2Dpos.addScaledVector(vUnit,2); 
	/*
	if(cam2Dcanvas.x<0 || cam2Dcanvas.x>ctx.width){
		camera.position.x = lastCameraPosition.x;
	}else{
		camera.position.x = cam2Dpos.x;
	}
	if(cam2Dcanvas.y<0 || cam2Dcanvas.y>ctx.height){
		camera.position.y = lastCameraPosition.y;
	}else{
		camera.position.y = cam2Dpos.y;
	}
	*/
	camera.position.x = cam2Dpos.x;
	camera.position.y = cam2Dpos.y;
	if(camera.position.x<-ctx.width/2+paddingX || camera.position.x>ctx.width/2-paddingX){
		camera.position.x = lastCameraPosition.x;
	}
	if(camera.position.y<-ctx.height/2+paddingY || camera.position.y>ctx.height/2-paddingY){
		camera.position.y = lastCameraPosition.y;
	}
	mouse3D_tmp.copy(mouse3D);

}