var last_fov = undefined; 
var last_width = undefined;
var last_height = undefined;
var last_near = undefined;
var last_far = undefined;
var last_z = undefined;

module.exports = function(app){ 

	var ag = app.game = app.game || {};

	ag.camera; 
	ag.scene; 
	ag.renderer;
	ag.geometry; 
	ag.material;
	ag.mesh;

	ag.height = ag.height || 400;

	ag.Rot360g = 2*Math.PI;
	ag.Rot180g = Math.PI;
	ag.Rot1g = Math.PI/180;
	ag. Rot10g = Math.PI/18;
	ag.Rot20g = Math.PI/9;
	ag.Rot30g = Math.PI/6;

	init();
	animate();
	 
	function init() { 
	    if(ag.ortho){
	    	ag.camera = new THREE.OrthographicCamera( window.innerWidth / - 2, window.innerWidth / 2, ag.height / 2, ag.height / - 2, 0.01,20 ); 
	    }else{
		    ag.camera = new THREE.PerspectiveCamera( 70, window.innerWidth / ag.height, 0.01, 10 );
		    ag.camera.position.z = 1;
	    }
	    

	 
	    ag.scene = new THREE.Scene();
	 
	    ag.geometry = new THREE.BoxGeometry( 0.2, 0.2, 0.2 );
	    ag.material = new THREE.MeshNormalMaterial();
	 
	    //ag.mesh = new THREE.Mesh( ag.geometry, ag.material );
	    //ag.scene.add( ag.mesh );
	 
	    ag.renderer = new THREE.WebGLRenderer( { antialias: true,canvas:jQuery('canvas')[0] } );
	    ag.renderer.setSize( window.innerWidth, ag.height ); 
	 
	}
	 
	function animate() {
	 
	    requestAnimationFrame( animate );
	 	if(ag.mesh){
		    ag.mesh.rotation.x += 0.01;
		    ag.mesh.rotation.y += 0.02;
	 	}
	 	var update_flag = false;
	 	if(ag.fov!==undefined && last_fov != ag.fov){
	 		console.log("a",last_fov+" - "+ag.fov);
	 		last_fov = ag.fov;
	 		update_flag = true;
	 	}
	 	if(ag.width!==undefined && last_width != ag.width){
	 		console.log("b",last_width+" - "+ag.width);
	 		last_width = ag.width;
	 		last_height = ag.height;
	 		update_flag = true;
	 	} 
	 	if(ag.near!==undefined && last_near != ag.near){
	 		console.log("d",last_near+" - "+ag.near);
	 		last_near = ag.near;
	 		update_flag = true;
	 	}
	 	if(ag.far!==undefined && last_far != ag.far){
	 		console.log("e",last_far+" - "+ag.far);
	 		last_far = ag.far;
	 		update_flag = true;
	 	}
	 	if(ag.z!==undefined && last_z != ag.z){
	 		console.log("f",last_z+" - "+ag.z);
	 		last_z = ag.z;
	 		update_flag = true;
	 	}
	 	if(update_flag){ 
	 		console.log("updating... flag");
	 		if(!ag.ortho){ 
		 		ag.camera.aspect = last_width / last_height;
		 		ag.camera.near = last_near;
		 		ag.camera.far = last_far;
		 		ag.camera.fov = last_fov;
		 		ag.camera.position.z = last_z;
	    		ag.renderer.setSize( last_width, last_height );
				ag.camera.updateProjectionMatrix();
	 		}else{ 
	 		}
    		console.log(ag.camera);
	 	}

    	if(ag.animation)ag.animation();
	    ag.renderer.render( ag.scene, ag.camera );
	 
	}
	function getRandomColor() {
	  	var letters = '0123456789ABCDEF';
	  	var color = '#';
	  	for (var i = 0; i < 6; i++) {
	    	color += letters[Math.floor(Math.random() * 16)];
	  	}
	  	return color;
	}
	ag.init = init;
	ag.animate = animate;
	ag.getRandomColor = getRandomColor;
	jQuery(window).resize(function(evt){
		if(ag.ortho){
			ag.height = window.innerHeight;
 			ag.camera.left = window.innerWidth / - 2;
			ag.camera.right = window.innerWidth / 2;
			ag.camera.top = ag.height / 2;
			ag.camera.bottom = ag.height / - 2; 
		}else{
			ag.camera.aspect = window.innerWidth / ag.height;  
		} 
		ag.camera.updateProjectionMatrix(); 
		ag.renderer.setSize( window.innerWidth, ag.height );
	});


};