var _mouse3DPos = new THREE.Vector3();
var t,x,y,z;
module.exports = function(_mousePos,camera,width,height){
	_mouse3DPos.set(
	    (_mousePos.x / width) * 2 - 1,
	    - (_mousePos.y / height) * 2 + 1,
	    0.5
	);
	_mouse3DPos.unproject(camera); 
	_mouse3DPos.sub(camera.position); 
	_mouse3DPos.normalize();

	t = (-1)*camera.position.z/_mouse3DPos.z; 
	x = camera.position.x + t*_mouse3DPos.x;
	z = camera.position.z + t*_mouse3DPos.z;
	y = camera.position.y + t*_mouse3DPos.y; 
	_mouse3DPos.set(x,y,z); 
	return _mouse3DPos;
};