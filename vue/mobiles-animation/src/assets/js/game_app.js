

var now = Date.now();
var then = Date.now();
var canvas;
var gl,tex;
var viewport_width,viewport_height;
var shaderProgram;
var positionBuffer;
var positionIndicesBuffer;
var texcoordBuffer;
var positionLocation;
var texcoordLocation;

var matrixLocation;
var textureLocation;

var canvas_width=2048;
var canvas_height=2048;

var plane_width=2048;
var plane_height=2048;

var textureInfo ={
    	width: 1,   // we don't know the size until it loads
    	height: 1 
  	};

var camera_matrix = gl_matrix.mat4.create();
var matrix_ortho;
var projectionMatrix;
var cameraMatrix;
var viewMatrix;
var viewProjectionMatrix;
var cameraPosition; 
var mousePosition2D = gl_matrix.vec2.create(); 
var viewport = gl_matrix.vec4.create(); 
var unProjectPoint = null; 
var threeCamPos = new THREE.Vector3(0,0,0.1);
var threeMouse3DPos = new THREE.Vector3(0,0,0.1);
var fieldOfViewRadians=60, aspect=1, zNear=0.1, zFar=10;
var canvasCameraPos = gl_matrix.vec2.create();
var canvasMousePos = gl_matrix.vec2.create();

var camera;

var keys={};

var _app = null;

module.exports = function(app){
	_app = app; 
	canvas = jQuery('canvas')[0]; 
	viewport_width = window.innerWidth;
	viewport_height = window.innerHeight; 
	canvas.width = viewport_width;
	canvas.height = viewport_height;
	viewProjectionMatrix = gl_matrix.mat4.create();
	viewMatrix = gl_matrix.mat4.create();
	cameraMatrix = gl_matrix.mat4.create();
	cameraPosition = gl_matrix.vec3.create();
	initWebGL(canvas);
	if(gl){
		requestAnimationFrame(render);
	}
	jQuery(window).resize(function(evt){
		if(gl)resize(evt);
	});
	jQuery(canvas).mousemove(function(evt){
		gl_matrix.vec2.set(mousePosition2D,evt.clientX,evt.clientY); 
	}); 
	jQuery(window).keydown(function(evt){
		keys[evt.key] = true; 
	});
	jQuery(window).keyup(function(evt){
		keys[evt.key] = false; 
	});
	resize();

	return {
		gl:gl, 
		canvas:canvas,
	};
}
function resize(evt){
	viewport_width = window.innerWidth;
	viewport_height = window.innerHeight; 
	//viewport_height = 400;
	canvas.width = viewport_width;
	canvas.height = viewport_height;
	//matrix_ortho = m4.orthographic(0, canvas.width, canvas.height, 0, -1, 1);
	changeViewPort();
	aspect = viewport_width*1.0/viewport_height;
	viewport[2] = viewport_width;
	viewport[3] = viewport_height;
	camera.viewport[2] = viewport_width;
	camera.viewport[3] = viewport_height;
	camera.update();
}
function unproject (vec, view, proj, viewport) {
 
	var dest = gl_matrix.vec3.create();//output
	var m = gl_matrix.mat4.create();//view * proj
	var im = gl_matrix.mat4.create();//inverse view proj
	var v = gl_matrix.vec4.create();//vector
	var tv = gl_matrix.vec4.create();//transformed vector
	 
	//apply viewport transform
	v[0] = (vec[0] - viewport[0]) * 2.0 / viewport[2] - 1.0;
	v[1] = (vec[1] - viewport[1]) * 2.0 / viewport[3] - 1.0;
	v[2] = vec[2];
	v[3] = 1.0;
	 
	//build and invert viewproj matrix
	gl_matrix.mat4.multiply(m,view,proj);
	if(!gl_matrix.mat4.invert(im,m)) { return null; }
	 
	gl_matrix.vec4.transformMat4(tv,v,im);
	if(v[3] === 0.0) { return null; }
	 
	dest[0] = tv[0] / tv[3];
	dest[1] = tv[1] / tv[3];
	dest[2] = tv[2] / tv[3];
	 
	return dest;
};

function changeViewPort(){
	if(gl)gl.viewport(0,0,viewport_width,viewport_height);
}
function calcMatrix(){
	return;
	/*
	projectionMatrix = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);
	gl_matrix.vec3.set(cameraPosition,threeCamPos.x,threeCamPos.y,threeCamPos.z);
	gl_matrix.mat4.identity(cameraMatrix);
	gl_matrix.mat4.translate(cameraMatrix,cameraMatrix,cameraPosition);
	gl_matrix.mat4.invert(viewMatrix,cameraMatrix);
	gl_matrix.mat4.mul(viewProjectionMatrix,projectionMatrix, viewMatrix);
	camera_matrix = m4.translate(viewProjectionMatrix,0,0,0);
	*/
	//camera_matrix = m4.orthographic(0, canvas.width, canvas.height, 0, 0, 1000);
	camera_matrix = m4.perspective(fieldOfViewRadians, aspect, zNear, zFar);
	//camera_matrix = m4.orthographic(-1.0, 1.0, -1.0, 1.0, 0.1, 100);
 
  	// this matrix will translate our quad to dstX, dstY
  	camera_matrix = m4.translate(camera_matrix, threeCamPos.x,threeCamPos.y,-threeCamPos.z);
}

function initWebGL(canvas) { 
  	try { 
    	gl = canvas.getContext("webgl",{ 
    				antialias: true,
                   	depth: true }) || 
    		canvas.getContext("experimental-webgl",{ 
    				antialias: true,
                   	depth: true });  
    	/*-----------------------------------------------------------------*/ 
    	gl.frontFace(gl.CCW); 
    	/*gl.frontFace(gl.CW);
    	gl.enable(gl.CULL_FACE);
		gl.cullFace(gl.BACK);*/
    	calcMatrix();
    	/*-----------------------------------------------------------------*/ 
    	camera = createCamera({
		  	fov: Math.PI,
		  	near: 0.01,
		  	far: 100,
		  	viewport:[0,0,viewport_width,viewport_height]
		});
		console.log("camera => ",camera);
    	/*-----------------------------------------------------------------*/ 
    	shaderProgram = gl_shader.initShaderProgram(
    		gl, 
    		jQuery('#drawImage-vertex-shader').html(),
    		jQuery('#drawImage-fragment-shader').html(),
    		function(err){console.log(err)}
    	);
    	window.shaderProgram = shaderProgram; 
    	if(shaderProgram){
    		positionLocation = gl.getAttribLocation(shaderProgram, "a_position");
  			texcoordLocation = gl.getAttribLocation(shaderProgram, "a_texcoord"); 

			// lookup uniforms
			matrixLocation = gl.getUniformLocation(shaderProgram, "u_matrix");
			textureLocation = gl.getUniformLocation(shaderProgram, "u_texture");

			// Create a buffer.
			positionBuffer = gl.createBuffer();
			gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

			// Put a unit quad in the buffer 
			var geoPlane = new Plane(plane_width,plane_height);
			geoPlane.position.set(0,0,-2);
			var positions = geoPlane.vertices;
			gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);

			/*positionIndicesBuffer = gl.createBuffer();
			gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, positionIndicesBuffer);
			gl.bufferData(
				gl.ELEMENT_ARRAY_BUFFER,
				new Uint16Array(geoPlane.indices), 
				gl.STATIC_DRAW); */
			// Create a buffer for texture coords
			texcoordBuffer = gl.createBuffer();
			gl.bindBuffer(gl.ARRAY_BUFFER, texcoordBuffer);

			// Put texcoords in the buffer
			var texcoords = [
				0, 0,
				0, 1,
				1, 0,
				1, 0,
				0, 1,
				1, 1,
			] 
			gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(texcoords), gl.STATIC_DRAW);
			createTextureOne();
    	}
  	}
  	catch(e) {
  		console.log(e);
  		console.log(e.stack);
  		gl=null;
  	}    
}

function drawImage(tex) {
	if(shaderProgram<=0 || tex<=0)return;
	gl.bindTexture(gl.TEXTURE_2D, tex);

	// Tell WebGL to use our shader program pair
	gl.useProgram(shaderProgram);

	// Setup the attributes to pull data from our buffers
	gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
	gl.enableVertexAttribArray(positionLocation);
	gl.vertexAttribPointer(positionLocation, 3, gl.FLOAT, false, 0, 0);
	gl.bindBuffer(gl.ARRAY_BUFFER, texcoordBuffer);
	gl.enableVertexAttribArray(texcoordLocation);
	gl.vertexAttribPointer(texcoordLocation, 2, gl.FLOAT, false, 0, 0); 

	// Set the matrix.
	if(camera && camera.projection){ 
		gl_matrix.mat4.copy(camera_matrix,camera.projection);
		gl.uniformMatrix4fv(matrixLocation, false, camera_matrix);
	}

	// Tell the shader to get the texture from texture unit 0
	gl.uniform1i(textureLocation, 0);

	// draw the quad (2 triangles, 6 vertices)
	gl.drawArrays(gl.TRIANGLES, 0, 6); 
}

function createTextureOne() {
  tex = gl.createTexture();
  gl.bindTexture(gl.TEXTURE_2D, tex);
 
  // let's assume all images are not a power of 2
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
  

  	var _canvas = document.createElement('canvas');
	var _ctx = _canvas.getContext('2d');
	_canvas.width=canvas_width;
	_canvas.height=canvas_height;
  	
  	textureInfo.canvas = _canvas;
  	textureInfo.ctx = _ctx; 
  	textureInfo.process = function(){
  		gl.bindTexture(gl.TEXTURE_2D, tex);
    	gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE,textureInfo.canvas); 
  	};
  	drawCanvas();
}
var _last_zindex = -1;
function render(){
	now = Date.now();
	if(_last_zindex!=_app.zindex){
		_last_zindex = _app.zindex;
		console.log("new zindex => ",_app.zindex);
		camera.translate([0,0,_app.zindex]);
		camera.lookAt([ 0, 0, 0 ])
		camera.update();
		console.log(camera);
	}
	draw();
	then = Date.now();
	requestAnimationFrame(render);
}
function processEvents(){
	var movH = 0;
	var movV = 0;
	var posX = 0.1;
	var posY = 0.1;
	if(keys['a'])movH+=posX; 
	if(keys['d'])movH-=posX; 
	if(keys['w'])movV+=posY; 
	if(keys['s'])movV-=posY; 
 	/*
 	threeCamPos.x+=movH; 
 	threeCamPos.y+=movV;
 	*/ 
 	threeCamPos.x-=movH; 
 	threeCamPos.y-=movV; 
 	_app.position='xy => '+threeCamPos.x+"-"+threeCamPos.y;
}
function draw(){  
  	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.clearColor(0.0, 0.0, 0.0, 1.0);  // Clear to black, fully opaque
  	gl.clearDepth(1.0);                 // Clear everything
  	gl.enable(gl.DEPTH_TEST);           // Enable depth testing
  	gl.depthFunc(gl.LEQUAL);   


	//processEvents();
	drawCanvas(); 
  	drawImage(
      tex); 
} 
function drawCanvas(){


	textureInfo.ctx.clearRect(0,0,textureInfo.canvas.width,textureInfo.canvas.height); 
	textureInfo.ctx.beginPath();
	textureInfo.ctx.fillStyle="#00FF00";
	textureInfo.ctx.fillRect(0,0,textureInfo.canvas.width,textureInfo.canvas.height);
	textureInfo.ctx.strokeStyle="#FF0000"; 
	textureInfo.ctx.lineWidth=5; 
	textureInfo.ctx.arc(textureInfo.canvas.width/2,textureInfo.canvas.height/2,textureInfo.canvas.height/2-50,0,2*Math.PI);  
	textureInfo.ctx.stroke();
	textureInfo.ctx.fillStyle="#0000FF"; 
	textureInfo.ctx.lineWidth=2; 
	textureInfo.ctx.font="20px Arial "; 
	textureInfo.ctx.fillText("Hola erick",textureInfo.canvas.width/2,textureInfo.canvas.height/2);
	textureInfo.ctx.fillRect(500,100,40,40); 
	textureInfo.ctx.fillStyle = '#FF00FF'; 

	/*
	gl_matrix.vec2.set(
		canvasCameraPos,
			canvas_width/2 - threeCamPos.x + viewport_width/2,
			canvas_height/2 - threeCamPos.y + viewport_height/2  
 	);
	gl_matrix.vec2.set(
		
		canvasMousePos,
		canvas_width/2 + mousePosition2D[0] - threeCamPos.x,
		canvas_height/2 + mousePosition2D[1]  - threeCamPos.y   
 	);

	textureInfo.ctx.fillRect(
		canvasCameraPos[0]-5,
		canvasCameraPos[1]-5,
		10,
		10
	);
	textureInfo.ctx.fillRect(
		canvasMousePos[0]-5,
		canvasMousePos[1]-5,
		10,
		10
	);
	textureInfo.ctx.beginPath();
	textureInfo.ctx.lineWidth = 5;
	textureInfo.ctx.moveTo(canvasCameraPos[0],canvasCameraPos[1]);
	textureInfo.ctx.lineTo(canvasMousePos[0],canvasMousePos[1]);
	textureInfo.ctx.stroke();
	textureInfo.ctx.closePath(); 
	*/
	textureInfo.process();
}
function Plane(width,height){  
	this.position = new THREE.Vector3();
	var position = [ 
		0,0,
		0,1,
		1,0,
		1,0, 
		0,1,
		1,1,
	];  
	for(var idx in position){
		position[idx] = position[idx]-0.5;
	}
	this.vertices = [];
	for(var idy=0;idy<position.length;idy+=2){
		position[idy]	*= width;
		position[idy+1]	*= height;
		this.vertices.push(position[idy]+this.position.x,position[idy+1]+this.position.y,this.position.z); 
	}  
} 